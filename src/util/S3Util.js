import { EnvConstants } from "../EnvConstants";
import { RequestUtil } from "./RequestUtil";
import { DonwloadUtil } from "./DonwloadUtil";

export class S3Util {

    static getTemporalPutUrl(config){
        const { folder, fileName, fnOk, fnError, contentType} = config;
        RequestUtil.postData(EnvConstants.GET_FW_URL_UPLOAD_PRESIGNED_URL_PUT(), {
            params: {
                folder,
                fileName,
                contentType
            }
        })
        .then( resp => {
            if (fnOk)
                fnOk(resp);
        })
        .catch(error=>{
            if (fnError)
                fnError(error);
        });
    }

    static getTemporalGetUrl(config){
        const { folder, fileName, fnOk, fnError} = config;
        RequestUtil.postData(EnvConstants.GET_FW_URL_UPLOAD_PRESIGNED_URL_GET(), {
            params: {
                folder: folder,
                fileName: fileName
            }
        })
        .then( resp => {
            if (fnOk)
                fnOk(resp);
        })
        .catch(error=>{
            if (fnError)
                fnError(error); 
        });
    }

    static async getTemporalGetUrlAsync(fileName, folder){
        let resp = await RequestUtil.postData(EnvConstants.GET_FW_URL_UPLOAD_PRESIGNED_URL_GET(), {
            params: {
                folder: folder,
                fileName: fileName
            }
        });
        return resp.dataObject.url;
    }

    static async getTemporalDeleteUrlAsync(fileName, folder){
        let resp = await RequestUtil.postData(EnvConstants.GET_FW_URL_UPLOAD_PRESIGNED_URL_DELETE(), {
            params: {
                folder: folder,
                fileName: fileName
            }
        });
        return resp.dataObject.url;
    }


    static async donwload(fileName, folder){
        let url = await S3Util.getTemporalGetUrlAsync(fileName,folder);
        console.log('fileName.getTemporalGetUrlAsync', url);
        DonwloadUtil.donwloadGet(url, fileName);
    }

    static async preview(fileName, folder){
        let url = await S3Util.getTemporalGetUrlAsync(fileName,folder);
        
        return await DonwloadUtil.getBase64(url, fileName);
    }

    static async getUrlS3(fileName, folder){
        let url = await S3Util.getTemporalGetUrlAsync(fileName,folder);
        return url;
    }

    static async delete(fileName, folder){
        let url = await S3Util.getTemporalDeleteUrlAsync(fileName,folder);
        console.log('fileName.getTemporalDeleteUrlAsync', url);
        fetch(url, {
            method: 'DELETE',
        })
        .then(res => res.json()) // or res.json()
        .then(res => console.log(res));
    }
    
    

    /*
    static downloadS3 = async(fileName, folder) => {
        S3Util.getTemporalGetUrl({
            folder: folder,
            fileName: fileName,
            fnOk: (resp) => {
                console.log(resp);
                if (resp?.dataObject?.url)
                    DonwloadUtil.donwloadGet(resp.dataObject.url, fileName);
            },
            error: (error) => {
                console.error(error);
            }
        });
    }*/

}

