// import { notification } from 'antd';
import swal from 'sweetalert';

export class NotificationUtil {
    static error = (msg) => {
        // notification['error']({
        //     message: 'Error',
        //     description: msg,
        // });
        swal(
            {
                title: 'Error',
                text: msg,
                icon: 'error'
            }
        );
    };

    static success = (msg) => {
        // notification['success']({
        //     message: 'Exito',
        //     description: msg,
        // });
        swal(
            {
                title: 'Exito',
                text: msg,
                icon: 'success'
            }
        );
    };
}


