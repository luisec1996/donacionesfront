export class LoadingUtil{

    static loadShow(){
        let load = document.querySelector('#loader');
        load.style.display= "block";
    }

    static loadHide(){
        let load = document.querySelector('#loader');
        load.style.display= "none";
    }
}