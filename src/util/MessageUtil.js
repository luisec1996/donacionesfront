import { message, notification } from "antd";
import swal from 'sweetalert';

export const MessageUtil = (type, title, msg) => {

    // notification[type]({
    //     message: title,
    //     description: msg,
    //     //placement: 'bottomRight' // posisiones
    // });

    swal(
        {
            title,
            text: msg,
            icon: type
        }
    );
}