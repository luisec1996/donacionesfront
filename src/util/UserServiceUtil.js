import { message } from 'antd';
import { StorageUtil } from '../util/StorageUtil';


export const LoginUtil = (params) => {
    const data = params.info;
    
    data.then(async(r) =>{
        if(r.dataObject.length > 0){
            await layout.menus(r.dataObject.menu);
        }
    })
}

const layout = {
        menus: async(lista) => {
            console.log(lista);
            lista.then(r => {
                        if (r.length > 0) {
                            const menus = layout.fnTransform(r);
                            StorageUtil.setItemObject('USER_MENU', {
                                title: 'Account',
                                fields: menus,
                            });
                        }
                    })
                    .catch(error => {
                        console.log('ERROR [MENU] => [LOGIN] again', error);
                    });
           
        },
        fnTransform: (menus) => {
            if (menus.length > 0) {
                const menuParents = menus.filter((menu, index) => menu.type === 1);
                menuParents.forEach(element => {
                    let tempChildren = menus.filter((child, idx) => child.parentId === element.routeId);
                    // order - children
                    tempChildren.sort((a, b) => a.numberOrder - b.numberOrder);
                    if (tempChildren.length > 0) {
                        element['fields'] = tempChildren;
                    }
                });
                const menuResult = menuParents.filter(e => (!e.parentId || e.parentId === '0') && e.fields?.length > 0);
                // order - parents
                menuResult.sort((a, b) => a.numberOrder - b.numberOrder);
                return menuResult || [];
            }
            return [];
        }
    }
    

