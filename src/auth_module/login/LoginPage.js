import React from 'react';
import './LoginPage.css';
import { NotificationUtil } from '../../util/NotificationUtil';
import { Link } from 'react-router-dom';
import { useForm } from '../../hooks/useForm';
import { SecurityUtil } from '../../util/SecurityUtil';
import logo from '../../assets/img/LOGO.png';

const LoginPage = (props) => {

	const [loginForm, handleInputChange] = useForm({ username: '', password: '', rememberMe: true });
  	// const history = useHistory();
	// const dispatch = useDispatch();

	const login = async (e) => {
		e.preventDefault();
		if (loginForm['username'] !== '' && loginForm['password'] !== '') {
			try {

        SecurityUtil.login(loginForm['username'],  loginForm['password']);

			} catch (error) {
				 NotificationUtil.error('Usuario o Contraseña erróneo');

			}
		} else {
      NotificationUtil.error('Ingrese Usuario y Contraseña');
		}
	}

	React.useEffect(() => {
		// setRemember(true);
	}, [loginForm])

	return (
		<>
			<div className="login__fondo__img">
				<div className="login__wrapper">
					<div className="login__form">
						<div className="login__content">    
            <img src={logo}  className="login__img"/>
							{/*<h1>Login</h1>*/}
							<form
								className="form-content"
								onSubmit={login}
								/*onFinish={onFinish}
								onFinishFailed={onFinishFailed}*/
							>
								<div className="group">
									<input type="text" name="username" autoComplete="off" onChange={handleInputChange} />
									<span className="highlight"></span>
									<span className="bar"></span>
									<label>Usuario</label>
								</div>

								<div className="group">
									<input type="password" name="password" onChange={handleInputChange} />
									<span className="highlight"></span>
									<span className="bar"></span>
									<label>Contraseña</label>
								</div>
								<div className="login__options">
							{/* 		<input type="checkbox" name="rememberMe" />
									<label htmlFor="remember_me"> Recuerdame </label> */}
									<Link to="!#"> ¿Olvidaste tu contraseña? </Link>
								</div>
								<button type="submit" className="login__button">Iniciar Sesión</button>
								<p>
									¿No tienes cuenta?
								<br />
									<Link to="/auth/register"> Suscríbete </Link>
								</p>
							</form>
						</div>
					</div>
				</div>
			</div>


		</>


	);
};

export default LoginPage;
