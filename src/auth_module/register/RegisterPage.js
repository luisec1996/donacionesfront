import React from 'react';
import './RegisterPage.css';
import { NotificationUtil } from '../../util/NotificationUtil';
import { useForm } from '../../hooks/useForm';
import logo from '../../assets/img/LOGO.png';
import { LoadUtil } from '../../framework/util/LoadUtil';
import { EnvConstants } from '../../EnvConstants';
import { SecurityUtil } from '../../util/SecurityUtil';
import { Link } from 'react-router-dom';
import { MessageUtil } from '../../util/MessageUtil';
import swal from 'sweetalert';

const RegisterPage = (props) => {

  const [registerForm, handleInputChange, reset] = useForm({ USUARIO_CREACION: 0, NOMBRES: '', APELLIDO_PATERNO: '', APELLIDO_MATERNO: '', ID_TIPO_DOCUMENTO: '', NRO_DOCUMENTO: '', FECHA_NACIMIENTO: '',DIRECCION: '', TELEFONO: '', SEXO: '', EMAIL: '', USUARIO: '', CONTRASENIA: '' });
  

	const register = (e) => {
		e.preventDefault();
		if (registerForm['NOMBRES'] !== '' && registerForm['APELLIDO_MATERNO'] !== '' && registerForm['APELLIDO_PATERNO'] !== '' && registerForm['ID_TIPO_DOCUMENTO'] !== '' && registerForm['NRO_DOCUMENTO'] !== '' && registerForm['FECHA_NACIMIENTO'] !== '' && registerForm['EMAIL'] !== '' && registerForm['USUARIO'] !== '' && registerForm['CONTRASENIA'] !== ''  ) {
			try {
                LoadUtil.loadByQueryId({
                    url: EnvConstants.GET_URL_WRITE(),
                    queryId: 22,
                    params: {...registerForm},
                    fnOk: (resp)=>{
                        swal("Estimado Usuario Se ha Registrado con Éxito!", {
                            icon: "success",
                            buttons: {
                              ok: true,
                            },
                        }).then((value) => {
                            switch (value) {
                           
                              case "ok":
                                SecurityUtil.login(resp?.dataObject?.USUARIO, resp?.dataObject?.CONTRASENIA);
                                break;
                              default:
                                swal("Got away safely!");
                            }
                          });

                        
                    }
                })
			} catch (error) {
				 NotificationUtil.error("Ingrese un Nro de documento correcto o el DNI ya se encuentra registrado.");
			}
		} 
        else {
            return MessageUtil('warning','Alerta!','Por favor, complete el formulario!')
            //NotificationUtil.error('Ingrese Usuario y Contraseña');
		}
	}

    const handleKeyPress = (event) => {
        if (event.key >= 0 && event.key <= 9) { 
            // console.log(event.key)
        }
        else{
            event.preventDefault();
        }
    }

    const handleKeyPressLetter = (event) => {
        if ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122)) { 
            //
        }
        else{
            event.preventDefault();
        }
    }

    const onChange = (event) => {
        let id_tipo_documento = event.target.value;

        if(id_tipo_documento === "2"){
            document.querySelector('#txtDocumento').maxLength = 8;
        }

        if(id_tipo_documento === "4"){
            document.querySelector('#txtDocumento').maxLength = 11;
        }

        if(id_tipo_documento === "3" || id_tipo_documento === "5"){
            document.querySelector('#txtDocumento').maxLength = 12;
        }

        if(id_tipo_documento === "1" || id_tipo_documento === "6" || id_tipo_documento === "7"){
            document.querySelector('#txtDocumento').maxLength = 15;
        }
        
        registerForm['ID_TIPO_DOCUMENTO'] = id_tipo_documento;
    }

	React.useEffect(() => {
		// setRemember(true);
	}, [registerForm])

	return (
		<>
			<div className="login__fondo__img">
				<div className="login__wrapper">
					<div className="register__form">
						<div className="register__content">
                            <img src={logo}  className="register__img"/>
                            <div className="register__container">
                                <div className="content">
                                <form onSubmit={register}>
                                    <div className="user-details">
                                    <div className="input-box">
                                        <span className="details">Nombres</span>
                                        <input type="text" name="NOMBRES" placeholder="Ingrese Nombre" required onChange={handleInputChange} onKeyPress={handleKeyPressLetter}/>
                                    </div>
                                    <div className="input-box">
                                        <span className="details">Apellido Paterno</span>
                                        <input type="text" name="APELLIDO_PATERNO" placeholder="Ingrese Apellido Paterno" required onChange={handleInputChange} onKeyPress={handleKeyPressLetter}/>
                                    </div>
                                    <div className="input-box">
                                        <span className="details">Apellido Materno</span>
                                        <input type="text" name="APELLIDO_MATERNO" placeholder="Ingrese Apellido Materno" required onChange={handleInputChange} onKeyPress={handleKeyPressLetter}/>
                                    </div>
                                    <div className="input-box">
                                        <span className="details">Tipo Documento</span>
                                        <select name="ID_TIPO_DOCUMENTO" onChange={onChange} > 
                                            <option value="0">--Seleccione--</option> 
                                            <option value="1">OTROS</option>
                                            <option value="2">DNI</option>
                                            <option value="3">CE</option>
                                            <option value="4">RUC</option>
                                            <option value="5">PASAPORTE</option>
                                            <option value="6">CDI</option>
                                            <option value="7">S/D</option>
                                        </select>
                                    </div>
                                    <div className="input-box">
                                        <span className="details">Nro Documento</span>
                                        <input type="text" name="NRO_DOCUMENTO" id="txtDocumento" placeholder="Ingrese Nro Documento" maxLength={8} required onChange={handleInputChange} onKeyPress={handleKeyPress}/>
                                    </div>
                                    <div className="input-box">
                                        <span className="details">Fecha Nacimiento</span>
                                        <input type="date" name="FECHA_NACIMIENTO" placeholder="Seleccione Fecha" required onChange={handleInputChange}/>
                                    </div>
                                    <div className="input-box">
                                        <span className="details">Dirección</span>
                                        <input type="text" name="DIRECCION" placeholder="Ingrese Dirección" required onChange={handleInputChange}/>
                                    </div>
                                    <div className="input-box">
                                        <span className="details">Teléfono</span>
                                        <input type="text" name="TELEFONO" placeholder="Ingrese Teléfono"  maxLength={11} required onChange={handleInputChange}/>
                                    </div>
                                    <div className="input-box">
                                        <span className="details">Sexo</span>
                                        <select name="SEXO" onChange={handleInputChange}> 
                                          <option value="0">--Seleccione--</option> 
                                          <option value="1">Masculino</option> 
                                          <option value="2">Femenino</option> 
                                        </select>
                                    </div>
                                    <div className="input-box">
                                        <span className="details">Email</span>
                                        <input name="EMAIL" type="email" placeholder="Ingrese Email" required onChange={handleInputChange} />
                                    </div>
                                    <div className="input-box">
                                        <span className="details">Usuario</span>
                                        <input type="text" name="USUARIO" placeholder="Ingrese Usuario" required onChange={handleInputChange} />
                                    </div>
                                    <div className="input-box">
                                        <span className="details">Contraseña</span>
                                        <input name="CONTRASENIA" type="password" placeholder="Ingrese Contraseña" required onChange={handleInputChange} />
                                    </div>
                                    
                                    </div>
                                    <div className="button">
                                        <button type="submit" className="login__button">Registrarse</button>
                                    </div>
                                    <div className="button">
                                    <Link to="/auth/login"> <button className="login__button_2">Volver</button> </Link>
                                    </div>
                                        
                                </form>
                                </div>
                            </div>
						</div>
					</div>
				</div>
			</div>
		</>
	);
};

export default RegisterPage;
