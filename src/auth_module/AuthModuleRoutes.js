import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import LoginPage from './login/LoginPage';
import RegisterPage from './register/RegisterPage';


export const AuthModuleRoutes = () => {
    return (
        <Switch>
            <Route exact path="/auth/login"  component={ LoginPage } />
            <Route exact path="/auth/register" component={ RegisterPage } />
            <Redirect to="/auth/login" />
        </Switch>
    )
}
