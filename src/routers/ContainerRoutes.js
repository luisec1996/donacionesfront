import React from 'react'
import {
    Switch,
    Route,
    useRouteMatch
  } from "react-router-dom";
import { ModulesRoutes } from '../modules/ModulesRoutes';


export const ContainerRoutes = () => {
    console.log('ContainerRoutes');
    return (
        <Switch>
            <Route path="/" component={ModulesRoutes} />    
        </Switch>
    )
}