import './fieldFw.css';
import React from 'react'
import { Col, Form} from 'antd';
import { Alert, Divider } from 'antd';
import SelectFw from '../select/SelectFw';
import { UploadFw } from '../upload/UploadFw';

import DateFw from '../date/DateFw';
import SearchFw from '../search/SearchFw';
import InputFw from '../input/InputFw';
import CheckboxFw from '../checkbox/CheckboxFw';
import RadioFw from '../radio/RadioFw';
import TextareaFw from '../textarea/TextareaFw';
import NumberFw from '../number/NumberFw';
import StaticFw from '../static/StaticFw';
import HiddenFw from '../hidden/HiddenFw';
import { FIELD_TYPE } from '../../../constants/constants';
import SwitchFw from '../switch/SwitchFw';
import { DraggerFw } from '../dragger/DraggerFw';
import CKEditorText from '../editor-text/CkEditor';
import PreviewFw from '../preview/PreviewFw';
import GalleryFw from '../gallery/GalleryFw';
import ObjectFw from '../object/ObjectFw';
import DecimalFw from '../decimal/DecimalFw';
import CalendarFw from '../calendar/CalendarFw';
import ButtonFormFw from '../button/ButtonFormFw';
 
export const createField = (config, restProps) => {
    switch(config.type) {
        case FIELD_TYPE.STATIC:
            return <StaticFw config={config} {...restProps}/>;
        case FIELD_TYPE.TEXT:
            return <InputFw config={config} {...restProps}/>;
        case FIELD_TYPE.DATE:
            return <DateFw config={config} {...restProps} />;
        case FIELD_TYPE.RADIO:
            return <RadioFw config={config} {...restProps} />;
        case FIELD_TYPE.CHECKBOX:
            return <CheckboxFw config={config} {...restProps} />;
        case FIELD_TYPE.TEXTAREA:
            return <TextareaFw config={config} {...restProps} />;
        case FIELD_TYPE.NUMBER:
            return <NumberFw config={config} {...restProps} />;
        case FIELD_TYPE.SELECT:
            return <SelectFw config={config} {...restProps}/>;
        case FIELD_TYPE.SEARCH:
            return <SearchFw config={config} {...restProps}/>;
        case FIELD_TYPE.UPLOAD:
            return <UploadFw config={config} {...restProps} />;
        case FIELD_TYPE.DRAGGER:
            return <DraggerFw config={config} {...restProps} />;
        case FIELD_TYPE.SWITCH:
            return <SwitchFw config={config} {...restProps} />;
        case FIELD_TYPE.DIVIDER:
            return <Divider className="divider" orientation="left">{config.label}</Divider>;
        case FIELD_TYPE.HIDDEN:
            return <HiddenFw config={config} {...restProps} />;
        case FIELD_TYPE.EDITOR:
            return <CKEditorText config={config} {...restProps} />; 
        case FIELD_TYPE.PREVIEW:
            return <PreviewFw config={config} {...restProps} />;
        case FIELD_TYPE.GALLERY:
            return <GalleryFw config={config} {...restProps} />;
        case FIELD_TYPE.OBJECT:
            return <ObjectFw config={config} {...restProps} />;
        case FIELD_TYPE.DECIMAL:
            return <DecimalFw config={config} {...restProps} />;
        case FIELD_TYPE.CALENDAR:
            return <CalendarFw config={config} {...restProps} />;
        case FIELD_TYPE.BUTTON:
            return <ButtonFormFw config={config} {...restProps} />;    
        default:
            return <Alert message={"Componente no existe"} type="error" />
    }
}

const FieldFw = ({config, ...restProps}) => {//{value, onChange, config, ...restProps}
    const [visibleState, setVisibleState] = React.useState(true);
    //const { value } = restProps;
    //console.log(value)//, 
    /*
    const [state, setState] = React.useState({value: value});
    if (value!=state.value){
        setState({value: value});
    }
    console.log(`render=>FieldFw[${config.name}]=> value:${value}, state:${state.value}`);*/
    config.setVisible = setVisibleState;

    if (config.isInput === false){
        return createField(config, restProps);
    }
    else {
    
        //console.log('pintando Form.Item');
        let item = (<Form.Item
            name = {config.name}
            label = {config.compact ? null : config.label}
            rules = {config.validations}
            style = {config.compact ? {margin: 0} : null}
            //hasFeedback={true}
            tooltip={config.tooltip}
            //messageVariables={{ label: 'good' }}
            //validateStatus="error"
            //className="compact" 
            hidden = {!visibleState}
        >
            {createField(config, restProps)}
        </Form.Item>);

        if (config.column && visibleState)
            return (
                <Col span={config.column*2}>
                    {item}
                </Col>
            );
        return item;
    }
}
export default FieldFw;
