import './NumberFw.css';
import React from 'react';
import { InputNumber } from 'antd';
import { FieldUtil } from '../helpers/FieldUtil';

const NumberFw = ({value, onChange, config, ...restProps}) => {
    const handleChange = (value,config) => {
        console.log(config);
        console.log(value);
        const reg = /^-?\d*(\.\d*)?$/;
        if ((!isNaN(value) && reg.test(value)) || value === '' || value === '-') {
        // this.props.onChange(value);
         
        }
    }
    // const pattern = /^\d+(\.\d{1,2})?$/;
    // const twoPointDecimal = number => Number(number).toFixed(2);

    return (
        <InputNumber
            ref={FieldUtil.createFieldRef(config)} 
            min={config?.min || 0}
            //defaultValue={0}
            //max={config?.max  || 0}
            disabled={config.readOnly}
            value={value}
            onChange={onChange}
            onPressEnter={config.onPressEnter} 
            onBlur={config.onBlur}
            onKeyDown={config.onKeyDown} 
            style={{width:'100%'}}
            //parser = {config.parser}
        />
        
        //max={config.max} bordered={false} pattern={config?.pattern.test() || pattern.test()}
    )
}
export default NumberFw;