
import React, { Component } from 'react';
import FileViewer from 'react-file-viewer';
import { FieldUtil } from '../helpers/FieldUtil';
import { CustomErrorComponent } from 'custom-error';

const PreviewFw = ({value, onChange, config, ...restProps}) => {
    
    
    const onError = e => {
        console.log(e, "error in file-viewer");
      };

    
    return (
        <FileViewer
        fileType={config.type_file}
        filePath={config.ruta_file}
        ref={FieldUtil.createFieldRef(config)} 
        errorComponent={CustomErrorComponent}
        onError={onError}
        />

    )
}
export default PreviewFw;