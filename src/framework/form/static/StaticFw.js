import './StaticFw.css';
import React from 'react';

const StaticFw = ({value, onChange, config, ...restProps}) => {
    const fnRender = () => {
        let parentData = config.getParentApi ? config.getParentApi().getData() : null;
        return config.fnRender ? config.fnRender(value, parentData) : null;
    }
    let result = fnRender();
    if (result)
        return result;

    if(config.decimal === undefined){
        config.decimal = false;
    }
    const twoPointDecimal = number => parseFloat(number).toFixed(2);

    if(config.decimal === true){
        return (
            <span className="static-fw">{value || twoPointDecimal(config.value)}</span>
        )
    }
    else{
        return (
            <span className="static-fw">{value || config.value}</span>
        )
    }
}
export default StaticFw;