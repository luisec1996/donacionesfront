import React from 'react'
import TextArea from 'antd/lib/input/TextArea';
import { FieldUtil } from '../helpers/FieldUtil';

const TextareaFw = ({value, onChange, config, ...restProps}) => {
    const [state, setState] = React.useState({value: value});
    const handleChange = (event) => {
        const { value } = event.target;
        setState({value: value});
        if (onChange) {
            onChange(event);
        }
    }

    if (value!=state.value){
        setState({value: value});
    }

    return (
        <TextArea 
            value={state.value}
            onChange={handleChange}
            disabled={config.readOnly}
            ref={FieldUtil.createFieldRef(config)}
            hidden={config.hidden}
            rows={config.rows} 
            placeholder={config.placeholder}
        />  
    )
}
export default TextareaFw;