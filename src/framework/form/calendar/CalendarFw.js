import React, { useEffect, useState } from 'react';
import { Calendar, Views, momentLocalizer } from 'react-big-calendar';
import 'moment/locale/es';
import moment from 'moment'
import 'react-big-calendar/lib/css/react-big-calendar.css'
import './CalendarFw.css'

const CalendarFw = ({config}) => {
    let events = [];
    events = config.value;


    const localizer = momentLocalizer(moment); 

    const [state, setState] = useState({events});

    //Cambiar el color de fondo del calendar
    // const ColoredDateCellWrapper = ({ children }) =>
    //     React.cloneElement(React.Children.only(children), {
    //     style: {
    //         backgroundColor: 'grey',
    //     },
    // })

    //Permite Insertar data en el calendar
    // const handleSelect = ({ start, end }) => {
    //     const title = window.prompt('New Event name')
    //     if (title)
    //       setState({
    //         events: [
    //           ...state.events,
    //           {
    //             start,
    //             end,
    //             title,
    //           },
    //         ],
    //       })
    //   }
     
    return (
        <div className="rbc-calendar">
            <Calendar
                selectable
                localizer={localizer}
                events={events}
                views={[Views.WEEK,Views.DAY]}
                defaultDate = {new Date()}
                defaultView={Views.WEEK}
                scrollToTime={new Date(2020, 1, 1, 6)}
                onSelectEvent={config.onClick}
                //onSelectSlot={handleSelect}
                messages={{
                    next: ">",
                    previous: "<",
                    today: "Hoy",
                    month: "Mes",
                    week: "Semana",
                    day: "Día"
                }}
                startAccessor="start"
                endAccessor="end"
                // components={{
                //     timeSlotWrapper: ColoredDateCellWrapper,
                // }}
                eventPropGetter={event => {
                    var backgroundColor = event.hexColor;
                    var style = {
                        borderWidth: 1,
                        color: 'black',
                        fontStyle: 'bold',
                        // borderLeftWidth: 4,
                        // borderLeftColor: backgroundColor,
                        backgroundColor: backgroundColor,
                        borderRadius: '5px 5px 5px 5px',
                        boxShadow: '2px 2px 2px 0 white',
                        // opacity: 0.8,
                        //left: 'auto !important',
                        width: '5%',
                    };
                    return {
                        style: style
                    };
                }
                }
                slotPropGetter={event => {
                    var style = {
                        width: 'auto !important',
                    };
                    return {
                        style: style
                    };
                }
                }
                step={30} //tiempo de separacion
                timeslots={1}
                min={new Date(0, 0, 0, 8, 0, 0)}
                max={new Date(0, 0, 0, 20, 0, 0)}
            />
        </div>
        
    )
}
export default CalendarFw;