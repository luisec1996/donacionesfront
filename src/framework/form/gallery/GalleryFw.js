import './galleryFw.css';
import React from 'react';
import ImageGallery from 'reactjs-image-gallery';

const GalleryFw = ({config, ...restProps}) => {
    //Así Tiene que venir la data
    // const data = [
    //     {
    //       url: "https://projectrepository-bureau.s3.amazonaws.com/Documents/Embarques/310748/Contenedores/378/14",
    //       title: "contenedor",
    //       thumbUrl: "https://projectrepository-bureau.s3.amazonaws.com/Documents/Embarques/310748/Contenedores/378/14"
    //     },
    // ];
    
    return (
        <ImageGallery images={config.value} />
    )
}
export default GalleryFw;