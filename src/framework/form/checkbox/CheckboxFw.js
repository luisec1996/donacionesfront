import React from 'react'
import { Checkbox } from 'antd';
import { CheckboxApi } from './CheckboxApi';

const valueProcessToArray = (config, value) => {
    if (config.multipleInString){
        if (value && value != ""){
            //console.log(`valueProcessFromArray ... '${value}'`);
            return value.split(",").map(item => parseInt(item));
        }
        return [];
    }
    return value;
}

const valueProcessFromArray = (config, value) => {
    if (config.multipleInString){
        if (value && value.length){
            //console.log(`valueProcessToArray ... '${value}'`);
            //debugger;
            return value.join();
        }
    }
    return value;
}


const CheckboxFw = ({value, onChange, config, ...restProps}) => {
    const [,forceRender] = React.useState(true);
    const apiRef = React.useRef(new CheckboxApi(config));
    //if (!apiRef.current) apiRef.current = new CheckboxApi(config);//inicializamos
    const getApi = () => apiRef.current;
    //getApi().setInnerValue(value);
    getApi().forceRender = forceRender;
    getApi().onChange = onChange;
    value = valueProcessToArray(config, value);

    const handleChange = (value, option) => {
        //getApi().setInnerValue(value);
        if (onChange) {
            value = valueProcessFromArray(config, value);
            onChange(value);
        }
    }
    React.useEffect(() => {
        getApi().firstMount();
        //console.log('SelectFw Primer Montado');
        return () => {
            getApi().unMount();
            apiRef.current = null;
            //console.log(`SelectFw[${config.name}].desmontado`);
        }
    }, []);
    return (
        <Checkbox.Group
            value={value}
            onChange={handleChange}
            options={getApi().getOptions()}
        />
    )
}
export default CheckboxFw;
