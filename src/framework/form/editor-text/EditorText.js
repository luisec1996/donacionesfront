
import React from 'react';
import { FieldUtil } from '../helpers/FieldUtil';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';

const EditorTextFw = ({value, onChange, config, ...restProps}) => {
    
    const fnRender = () => {
        let parentData = config.getParentApi ? config.getParentApi().getData() : null;
        return config.fnRender ? config.fnRender(value, parentData) : null;
    }
    let result = fnRender(); 
    if (result)
        return result;

    const modules ={
        toolbar: [
            ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
            ['blockquote', 'code-block'],
        
            [{ 'header': 1 }, { 'header': 2 }],               // custom button values
            [{ 'list': 'ordered'}, { 'list': 'bullet' }],
            [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
            [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
            [{ 'direction': 'rtl' }],                         // text direction
        
            [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
            [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
        
            [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
            [{ 'font': [] }],
            [{ 'align': [] }],
        
            ['clean']    
        ],
    }
    
    return (
        <ReactQuill 
            theme="snow" 
            value={value} 
            onChange={onChange} 
            ref={FieldUtil.createFieldRef(config)} 
            onPressEnter={config.onPressEnter} 
            onBlur={config.onBlur}
            onKeyDown={config.onKeyDown}
            modules={modules}
            />
    )
}
export default EditorTextFw;