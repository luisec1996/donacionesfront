import React, { useState } from 'react';
import CKEditor from 'ckeditor4-react-advanced';
// import JoditEditor from "jodit-react";

const CKEditorText = ({value, onChange, config}) => {

    const [state, setState] = useState({data: ''});

    const handleChange = (event) => {
        setState({data: event.editor.getData()});
        console.log(state.data)
    }

    const handleInit = (event) => {
        setState({data: value});
    }
    return (
            <CKEditor
            name={config.name}
            ref={config.ref} 
            data={state.data}
            onInstanceReady={ handleInit }
            onChange={ config.onChange }
            // onBlur={ ( event, editor ) => {
            //     //console.log( 'Blur.', editor );
            // } }
            // onFocus={ ( event, editor ) => {
            //     //console.log( 'Focus.', editor );
            // } }
            />

            
        ) || ( <div>Editor loading</div> )
}
export default CKEditorText;