import { UserOutlined } from '@ant-design/icons';
import './UserBarFw.css';
import { Popover } from 'antd';
import Avatar from 'antd/lib/avatar/avatar';
import React from 'react';
import { StorageUtil } from '../../util/StorageUtil';
import { WindowUtil } from '../../util/WindowUtil';
import { SecurityUtil } from '../../util/SecurityUtil';
import {  Typography  } from 'antd';

const { Text, Link } = Typography;

const UserBarFw = () => {
    let config = null;
    //const text=(<div className="user-bar-menu-title">Cuenta</div>);
    //const content = "content"

    

    const createContent = () => {
        const onClick = () => {
            SecurityUtil.setLogout();
        };
        if (config.fields){
            return config.fields.map((field, index) => (
                <div key={index} className="user-bar-field" onClick={onClick} > <span  className={`user-bar-field-icon ${field.icon}`}></span>  {field.label} </div>
            ));
        }
        return "Nada";
    }

    if (StorageUtil.exist('USER_BAR')){
       config = StorageUtil.getItemObject('USER_BAR');
       
       /*config.data ={ SecurityUtil.getUser()[0].lastname1 +" " + SecurityUtil.getUser()[0].lastname2+", " + SecurityUtil.getUser()[0].name + " "};*/
        /*<Text strong>{SecurityUtil.getUser()[0].lastname1}</Text> */        
    }
    else
        return null;

    return (
        
        <div>
            
            
            
            <Popover className="user-bar" placement="bottomRight" title={config.title} content={createContent(config)} trigger="click">
                <Avatar  style={{
                                color: '#ffffff',
                                backgroundColor: '#0b1849',
                            }} >
                                {SecurityUtil.getUser()[0].name?.charAt(0)}
                    </Avatar>
            </Popover>
            
        </div>
    );
};
export default UserBarFw;