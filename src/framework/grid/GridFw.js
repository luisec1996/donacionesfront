import './GridFw.css';
import React from 'react';
import log from 'loglevel';
import { Table } from 'antd';
import { createColumn } from './helpers/createColumn';
import { ICON } from '../../constants/iconConstants';
import { GridApi } from './GridApi';
import { EditableCell } from './editable/EditableCell';
import ContainerFw from '../container/ContainerFw';
import { EditableRow } from './editable/EditableRow';

export const COLUMN_DEFAULT = {
    VIEW    :{name:'view',   type: 'button', icon: ICON.VIEW, color: '#0B1849', title: 'Ver'}, 
    DELETE  :{name:'delete', type: 'button', icon: ICON.DELETE, color: '#0B1849', title: 'Eliminar', confirm: {title: 'Eliminación', message: '¿Esta seguro que desea eliminar el registro?'}}, 
    EDIT    :{name:'edit',   type: 'button', icon: ICON.EDIT, color: '#0B1849', title: 'Editar'},
    REPORT  :{name:'report', type: 'button', icon: ICON.REPORT, color: '#0B1849'},
    MAIL    :{name: 'mail',  type: 'button', icon: ICON.MAIL, color: '#0B1849'},
    DROPBOX :{name: 'dropbox', type: 'button', icon: ICON.DROPBOX, color: '#0B1849'},
    OPTIONS :{name: 'unorderedList', type: 'button', icon: ICON.OPTION, color: '#0B1849'},
    EXCEL   :{name: 'excel', type: 'button', icon: ICON.EXCEL, color: '#0B1849'},
    SETTING :{name: 'setting', type: 'button', icon: ICON.SETTING, color: 'black', title: 'Configuración'},
    ORDER   :{name: 'order', type: 'button', icon: ICON.ORDER, color: '#0B1849'},
    ADD     :{name:'add',type:'button',icon:ICON.PLUS, color: '#0B1849'},
    COPY    :{name:'copy',type:'button',icon:ICON.COPY, color: '#0B1849'},
    PDF     :{name:'pdf',type:'button',icon:ICON.PDF, color: '#0B1849'},
    CHECK   :{name:'check',type:'button',icon:ICON.CHECK, color: '#0B1849'},
    CANCEL  :{name:'cancel',type:'button',icon:ICON.CANCEL, color: '#0B1849'},
    EYE     :{name:'eye',type:'button',icon:ICON.EYE, color: '#0B1849'},
    DOWNLOAD    :{name:'download',type:'button',icon:ICON.DOWNLOAD, color: '#0B1849'},
    EXPERIMENT  :{name:'experiment',type:'button',icon:ICON.EXPERIMENT, color: '#0B1849'},
    DOCUMENT    :{name: 'document', type:'button',icon:ICON.NEW, color: '#0B1849'},
    SYNC        :{name: 'sync', type: 'button', icon: ICON.SYNC, color: '#0B1849'},
    COMMENT     :{name: 'comment', type: 'button', icon: ICON.COMMENT, color: '#0B1849'},
    DOLLAR      :{name: 'prices', type: 'button', icon: ICON.DOLLAR, color: '#0B1849'},
};

export const SELECTION_TYPE = {
    MULTIPLE: 'checkbox',
    SINGLE: 'radio'
}

const createGrid = (name, config, state, handleTableChange, getApi) => {
    const components = !getApi().isEditable() ? null : {
        body: {
            row: EditableRow,
            cell: EditableCell
        }
    };

    return (
        <Table 
            //scroll={{ x: 100, y: 100 }}
            className = "table-head-bv"
            size="small"
            rowClassName={() => 'grid-fw-row'}
            components={components}
            columns={createColumn(config, getApi)} 
            //dataSource={getApi().getDataContainer().getData()}
            dataSource={state.dataList}
            hasData={getApi().getDataContainer().hasData()}
            loading={state.loading}
            pagination={
                {
                    ...state.pagination,
                    showSizeChanger: config.pagination?.showSizeChanger
                }
            }
            onChange={handleTableChange}
            scroll={config.scroll || 'auto'}//{config.scroll}//fixed//scroll//{x: 'max-content'}
            locale={{emptyText:'No hay datos'}}
            onRow={(record, rowIndex) => {
                config['api'] = getApi;
                return {
                    record, 
                    config,
                    onClick: event => { 
                        log.debug(`GridFw[${name}].rowClick[${record?.key}]`, event);
                    }, 
                    //onDoubleClick: event => { console.log('onDoubleClick'); }, // double click row
                    //onContextMenu: event => { console.log('onContextMenu'); }, // right button click row
                    //onMouseEnter: event => { console.log('onMouseEnter'); }, // mouse enter row
                    //onMouseLeave: event => { console.log('onMouseLeave'); }, // mouse leave row
                };
            }}
            onHeaderRow={(columns, index) => {
                return {
                    onClick: () => {}, // click header row
                };
            }}

            rowSelection={getApi().createRowSelection()}
        />
    );
}

const GridFw = ({name, getParentApi, config}) => {
    log.debug(`GridFw[${name}] => Iniciando`);
    const apiRef = React.useRef(null);
    if (!apiRef.current){
        apiRef.current = new GridApi(name, null, config, null);
    }
    const [,forceRender] = React.useState(false);
    const getApi = () => apiRef.current;
    const { pagination } = config;
    const [state, setState] = React.useState({
        loading: false,
        pagination: !pagination? false : {
            current: pagination.current,
            pageSize: pagination.pageSize,
            showSizeChanger: pagination.showSizeChanger
        }
    });
    getApi().state = state;
    getApi().setState = setState;
    getApi().forceRender = forceRender;
    
    const handleTableChange = (toPage, filter, sorter) => {
        if (pagination){
            if (pagination.server){
                getApi().load(filter, sorter, toPage);
            }
            else{
                setState({
                    ...state,
                    pagination: toPage
                });
            }
        }
    };

    React.useEffect(() => {
        log.debug(`GridFw[${name}] => Mounting`);
        if (config.autoLoad)
            getApi().load(); 
        if (getParentApi)
            getParentApi().addComponent(name, getApi());
        return () => {
            log.debug(`GridFw[${name}] => Unmounting`);
        }
    }, []);

    //debugger;
    return (
        <ContainerFw
            childName = {name}
            getParentApi = {getApi}
            config = {config}
        >
            {createGrid(name, config, state, handleTableChange, getApi)}
        </ContainerFw>
    );
};
export default GridFw;
