import { ExclamationCircleOutlined } from "@ant-design/icons";

import './ButtonFw.css';
import React from 'react'
import confirm from 'antd/lib/modal/confirm';
import { Button, Tooltip } from 'antd';
import {useHistory} from 'react-router-dom';
import ConfirmFw from '../confirm/ConfirmFw';
import { RequestUtil } from '../../util/RequestUtil';
import { ButtonApi } from './ButtonApi';
// import Button from "antd-button-color";
import 'antd/dist/antd.css'; // or 'antd/dist/antd.less'
import 'antd-button-color/dist/css/style.css';

const createLabel = (config) => {
    return (config['label'] || '');
};

const createButton = (config, fnOnClick, loadingState) => {
    if (config.subType === 'icon'){
        if(config.visible === true){
            return (
                <Tooltip placement="left" title={config.title} color={config.color}> 
                    <span className="button-fw-icon" style={{color: config.color, textAlign: 'center', display: 'none'}} onClick={fnOnClick} value={config.value}>{config.icon}</span>
                </Tooltip>
            )
        }
        else{
            return (
                <Tooltip placement="left" title={config.title} color={config.color}> 
                    <span className="button-fw-icon" style={{color: config.color, textAlign: 'center'}} onClick={fnOnClick} value={config.value} >{config.icon}</span>
                </Tooltip>
            )
        }
        
    }
    else
        return ( 
            <Button 
                icon={config.icon} 
                onClick={fnOnClick} 
                type={config.type}
                disabled={config.readOnly}
                loading={loadingState}
                value={config.value}
                //href={config.link}
            >
                {createLabel(config)}
            </Button>
        
        
    )
}

const configureProcess = (config) => {
    const process = config['process'];
    config['onClick']  = () => {
        console.log('entrando');
        const params = typeof process.filter === 'function' ?  process.filter() : (process.filter || {});
        RequestUtil.postData(process.url, params)
        .then( result => {
            if (process.fnOk){
                process.fnOk(result, params);
            }
        })
        .catch(error=>{
            
        })

    }
}

const configureLink = (config,history) => {
    const link = config['link'];
    if (typeof link === 'function')
        config['onClick']  = () => {
            if(config['alert'] === true){
                confirm({
                    title: config['message'] || 'Está Seguro de retornar ?',
                    icon: <ExclamationCircleOutlined />,
                    onOk() { history.push(link()) },
                    onCancel() { },
                });
            }
            else{
                history.push(link())
            }
        };
    else if (link){
        config['onClick']  = () => {
            if(config['alert'] === true){
                confirm({
                    title: config['message'] || 'Está Seguro de retornar ?',
                    icon: <ExclamationCircleOutlined />,
                    onOk() { history.push(link) },
                    onCancel() { },
                });
            }
            else{
                history.push(link)
            }
        } 
    }
}

const ButtonFw = ({name, config}) => {
    const history = useHistory();
    const { confirm, link, process, visible, onVisible} = config;
    const [state, setState] = React.useState({visible: (visible === undefined)? true : visible });
    const [loadingState, setLoadingState] = React.useState(false);
    const apiRef = React.useRef(new ButtonApi(config, state, setState));




    /* resolvemos cuando es un button link */
    if (link)
        configureLink(config, history);
        
    if (process)
        configureProcess(config);
        


    const onClick = config.onClick;
    const  fnOnClick = !onClick ? null : async() => {
        if (onClick){
            setLoadingState(true);
            await onClick();
            setLoadingState(false);
        }
            
    } 


    let html = null;
    if (confirm){
        const confirmConfig = {
            message: confirm.message,
            onOkClick: fnOnClick
        };
        config['onClick'] = null;
        html = (
            <ConfirmFw 
                config={confirmConfig}
            >
                {createButton(config , null, loadingState)}
            </ConfirmFw>
            );
    }
    else{
        html = createButton(config, fnOnClick, loadingState);
    }

    React.useEffect(() => {
        if (config.handleAddField){
            config.handleAddField(config.name, apiRef.current);
        } 
        return () => {
            //console.log('Combo desmontado');
        }
    }, [])

    if (state.visible)
        return (html);
    else
        return <></>;
    
}

export default ButtonFw;