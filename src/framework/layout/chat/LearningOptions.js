import React from "react";

import "./LearningOptions.css";

const LearningOptions = (props) => {
  const options = [
    {
        text: "Consultar Estado de Donación",
        handler: props?.actionProvider?.handleConsultarEstado,
        id: 1,
    },
    { 
        text: "Consultar Reporte de Envíos", 
        handler: props?.actionProvider?.handleConsultarReportes,
        id: 2 
    },
  ];

  const optionsMarkup = options.map((option) => (
    <button
      className="learning-option-button"
      key={option.id}
      onClick={option.handler}
    >
      {option.text}
    </button>
  ));

  return <div className="learning-options-container">{optionsMarkup}</div>;
};

export default LearningOptions;