import { createChatBotMessage } from 'react-chatbot-kit';
import LearningOptions from './LearningOptions';
import LinkList from './LinkList';
import ListDonaciones from './ListDonaciones';
import ListFinal from './ListFinal';
import ListReportes from './ListReportes';
import ReporteOptions from './ReporteOptions';


const config = {
    botName: "DonaBot",
    initialMessages: [createChatBotMessage("¿En qué podemos ayudarte?", {
        widget: "learningOptions",
      })],
    customStyles: {
      botMessageBox: {
        backgroundColor: "#376B7E",
      },
      chatButton: {
        backgroundColor: "#376B7E",
      },
    },
    widgets: [
        {
          widgetName: "reporte-donacion-1",
          widgetFunc: (props) => <ReporteOptions {...props} />,
        },
        {
          widgetName: "learningOptions",
          widgetFunc: (props) => <LearningOptions {...props} />,
        },
        {
          widgetName: "status",
          widgetFunc: (props) => <ListDonaciones {...props} />,
        },
        {
          widgetName: "final",
          widgetFunc: (props) => <ListFinal {...props} />,
        },
        {
          widgetName: "reporte",
          widgetFunc: (props) => <LinkList {...props} />,
          props: {
            options: [
              {
                text: "Introduction to JS",
                url:
                  "https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-javascript/",
                id: 1,
              },
              {
                text: "Mozilla JS Guide",
                url:
                  "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide",
                id: 2,
              },
            ],
          },
        },
        {
          widgetName: "lista_reportes",
          widgetFunc: (props) => <ListReportes {...props} />,
        },
        {
          widgetName: "donacion1",
          widgetFunc: (props) => <LinkList {...props} />,
          props: {
            options: [
              {
                text: "Recibido",
                url:
                  "",
                id: 1,
              },
            ],
          },
        },
        {
          widgetName: "donacion2",
          widgetFunc: (props) => <LinkList {...props} />,
          props: {
            options: [
              {
                text: "Finalizado",
                url:
                  "",
                id: 1,
              },
            ],
          },
        },
    ],
};

export default config;