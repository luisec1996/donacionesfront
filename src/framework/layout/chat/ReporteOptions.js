import React from "react";

import "./LearningOptions.css";

const ReporteOptions = (props) => {
  const options = [
    {
        text: "Descargar Reporte",
        handler: props?.actionProvider?.handleDescargarReporte,
        id: 1,
    },
  ];

  const optionsMarkup = options.map((option) => (
    <button
      className="learning-option-button"
      key={option.id}
      onClick={option.handler}
    >
      {option.text}
    </button>
  ));

  return <div className="learning-options-container">{optionsMarkup}</div>;
};

export default ReporteOptions;