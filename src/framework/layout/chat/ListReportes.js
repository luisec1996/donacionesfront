import React from "react";

import "./LearningOptions.css";

const ListReportes = (props) => {
  const options = [
    {
        text: "Donación 1 - 06/11/2021",
        handler: props?.actionProvider?.handleReporteDonacion1,
        id: 1,
    },
    { 
        text: "Donación 2 - 06/11/2021", 
        handler: props?.actionProvider?.handleReporteDonacion1,
        id: 2 
    },
  ];

  const optionsMarkup = options.map((option) => (
    <button
      className="learning-option-button"
      key={option.id}
      onClick={option.handler}
    >
      {option.text}
    </button>
  ));

  return <div className="learning-options-container">{optionsMarkup}</div>;
};

export default ListReportes;