class MessageParser {
    constructor(actionProvider) {
      this.actionProvider = actionProvider;
    }
  
    parse(message) {
      const lowerCaseMessage = message.toLowerCase();
  
    //   if (lowerCaseMessage.includes("hello")) {
    //     this.actionProvider.greet();
    //   }
  
      if (lowerCaseMessage.includes("estado") || lowerCaseMessage.includes("donación")) {
        this.actionProvider.handleConsultarEstado();
      }

      if (lowerCaseMessage.includes("reporte")) {
        this.actionProvider.handleConsultarReportes();
      }

      if (lowerCaseMessage.includes("donacion1") ) {
        this.actionProvider.handleDonacion1();
      }

      if (lowerCaseMessage.includes("donacion2")) {
        this.actionProvider.handleDonacion2();
      }

      if (lowerCaseMessage.includes("si") ) {
        this.actionProvider.handleSi();
      }

      if (lowerCaseMessage.includes("no")) {
        this.actionProvider.handleNo();
      }

      if (lowerCaseMessage.includes("6")) {
        this.actionProvider.handleBuscarReporte();
      }
    }
  }
  
export default MessageParser;