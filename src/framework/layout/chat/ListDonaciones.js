import React from "react";

import "./LearningOptions.css";

const ListDonaciones = (props) => {
  const options = [
    {
        text: "Donación 31/10/2021",
        handler: props?.actionProvider?.handleDonacion1,
        id: 1,
    },
    { 
        text: "Donación 02/11/2021", 
        handler: props?.actionProvider?.handleDonacion2,
        id: 2 
    },
  ];

  const optionsMarkup = options.map((option) => (
    <button
      className="learning-option-button"
      key={option.id}
      onClick={option.handler}
    >
      {option.text}
    </button>
  ));

  return <div className="learning-options-container">{optionsMarkup}</div>;
};

export default ListDonaciones;