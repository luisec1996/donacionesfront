import React from "react";

import "./LearningOptions.css";

const ListFinal = (props) => {
  const options = [
    {
        text: "Si",
        handler: props?.actionProvider?.handleSi,
        id: 1,
    },
    { 
        text: "Finalizar Chat", 
        handler: props?.actionProvider?.handleNo,
        id: 2 
    },
  ];

  const optionsMarkup = options.map((option) => (
    <button
      className="learning-option-button"
      key={option.id}
      onClick={option.handler}
    >
      {option.text}
    </button>
  ));

  return <div className="learning-options-container">{optionsMarkup}</div>;
};

export default ListFinal;