import jsPDF from 'jspdf'
import autotable from 'jspdf-autotable'
import LOGO from '../../../../assets/img/LOGO.png'

import donation from '../../../../assets/img/donation.jpg'

const ReporteDonacion = () => {

    const doc = new jsPDF();
    doc.addImage(LOGO, 'JPEG', 20, 10, 60, 30)
    doc.setFontSize(7);
    //Rectangulo fecha
    doc.setDrawColor(0);
    doc.setFillColor(234,234,234);
    doc.roundedRect(125, 47,80, 10, 1, 1, 'F'); 
    doc.text(126,50,'Fecha y hora: 06/11/2021');
    
    
    doc.setDrawColor(0);
    doc.setFillColor(234,234,234);
    doc.roundedRect(10, 65, 109, 18, 1, 1, 'F'); 
    doc.text(11,70,'Nombre del Donador: Luis Espino Cuadros'); 
    doc.text(11,75,'Domicilio: Lima - Lima - Lima');


    doc.addImage(donation, 'JPEG', 10, 80, 200, 80)

  
      // if(type_return == 1){
      //   if(props.ID_TIPO_DOCUMENTO == 1){
      //     return doc.save(`Boleta_${props.SERIE_DOCUMENTO}.pdf`);
      //   }
      //   else{
          return doc.save(`Reporte.pdf`);
      //   }
      // }
      // else{
      //     return btoa(doc.output());
      // }
    };

export default ReporteDonacion;
