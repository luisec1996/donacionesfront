import { createChatBotMessage } from 'react-chatbot-kit';
import ReporteDonacion from './pdf/ReporteDonacion';

class ActionProvider {
    constructor(createChatbotMessage, setStateFunc, createClientMessage) {
      this.createChatbotMessage = createChatbotMessage;
      this.setState = setStateFunc;
      this.createClientMessage = createClientMessage;
    }
    props = this.props;

    handleConsultarEstado = () => {
        const message = createChatBotMessage(
          "...",
          {
            widget: "status",
          }
        );
    
        this.updateChatbotState(message);
    };

    handleConsultarReportes = () => {
        const message = createChatBotMessage(
          "Ingrese Fecha de Donación a consultar",
          // {
          //   widget: "report",
          // }
        );
    
        this.updateChatbotState(message);
    };

    handleBuscarReporte = () => {
      const message = createChatBotMessage(
        "...",
        {
          widget: "lista_reportes",
        }
      );
  
      this.updateChatbotState(message);
  };

    handleDonacion1 = () => {
      const message = createChatBotMessage(
        "Su donación se encuentra:",
        {
          widget: "donacion1",
        }
      );
  
      this.updateChatbotState(message);

      setTimeout(() => {
        const message2 = createChatBotMessage(
          "Algo más en que podemos ayudarle?",
          {
            widget: "final",
          }
        );
    
        this.updateChatbotState(message2);

      }, 3000)
      
  };

  handleDonacion2 = () => {
      const message = createChatBotMessage(
        "Su donación se encuentra:",
        {
          widget: "donacion2",
        }
      );
  
      this.updateChatbotState(message);
  };

  handleSi = () => {
    const message = createChatBotMessage(
      {
        widget: "learningOptions",
      }
    );

    this.updateChatbotState(message);
};

handleNo = () => {
  const message = createChatBotMessage(
    "Gracias!"
  );

  this.updateChatbotState(message);
};


handleReporteDonacion1 = () => {
  const message = createChatBotMessage(
    "Haga Click en descargar para visualizar su reporte",
    {
      widget: "reporte-donacion-1",
    }
  );

  this.updateChatbotState(message);

  // setTimeout(() => {
  //   const message2 = createChatBotMessage(
  //     "Algo más en que podemos ayudarle?",
  //     {
  //       widget: "final",
  //     }
  //   );

  //   this.updateChatbotState(message2);

  // }, 3000)
  
};

handleDescargarReporte = () => {

  ReporteDonacion();
  // const message = createChatBotMessage(
  //   "...",
  //   {
  //     widget: "reporte-donacion-1",
  //   }
  // );

  // this.updateChatbotState(message);

  setTimeout(() => {
    const message2 = createChatBotMessage(
      "Algo más en que podemos ayudarle?",
      {
        widget: "final",
      }
    );

    this.updateChatbotState(message2);

  }, 3000)
  
};


      updateChatbotState(message) {
        // NOTICE: This function is set in the constructor, and is passed in from the top level Chatbot component. The setState function here actually manipulates the top level state of the Chatbot, so it's important that we make sure that we preserve the previous state.
    
        this.setState((prevState) => ({
          ...prevState,
          messages: [...prevState.messages, message],
        }));
      }
}

export default ActionProvider;