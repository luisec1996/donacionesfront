import './PrincipalLayout.css';
import React, { useState } from 'react'
import ContainerLayout from './ContainerLayout';
import { Layout} from 'antd';
import HeaderLayout from './header-layout/HeaderLayout';
import MenuLayout from './menu-layout/MenuLayout';
import logo from '../../assets/img/LOGO.png';
import Chatbot from 'react-chatbot-kit'
import 'react-chatbot-kit/build/main.css'
import config from './chat/config';
import ActionProvider from './chat/ActionProvider';
import MessageParser from './chat/MessageParser';
import Fade from "react-reveal/Fade";
import Flip from "react-reveal/Flip";
const {  Footer, Sider } = Layout;

const PrincipalLayout = () => {
  const [showBot, toggleBot] = useState(false);

  const [state, setState] = useState({
      collapsed: false
  });

  const headerConfig = {
    state,
    setState,
  }

  return (
    <>
      
      <Layout>
        <Sider
          trigger={null} 
          collapsible 
          collapsed={state.collapsed}
          style={{
            overflow: 'auto',
            height: '100vh',
            position: 'fixed',
            left: 0,
          }}
        >
         <div className="logo" > 
            <img src={logo} alt="" />
          </div>
          <MenuLayout />
        </Sider>
        <Layout style={state.collapsed ? { marginLeft: 80 } : { marginLeft: 200 } }>
          <HeaderLayout  
            config = {headerConfig}
          />
          <ContainerLayout/>

{showBot && (
        <Fade big>
          <div className="app-chatbot-container">
          <Chatbot config={config}
        messageParser={MessageParser}
        actionProvider={ActionProvider}
        headerText='Conversación con Dona bot'
        placeholderText='Escriba su mensaje aquí'
        />
          </div>
        </Fade>
      )}
      <Flip left cascade>
        <button
          className="app-chatbot-button"
          onClick={() => toggleBot((prev) => !prev)}
        >
          <div>Bot</div>
          <svg viewBox="0 0 640 512" className="app-chatbot-button-icon">
            <path d="M192,408h64V360H192ZM576,192H544a95.99975,95.99975,0,0,0-96-96H344V24a24,24,0,0,0-48,0V96H192a95.99975,95.99975,0,0,0-96,96H64a47.99987,47.99987,0,0,0-48,48V368a47.99987,47.99987,0,0,0,48,48H96a95.99975,95.99975,0,0,0,96,96H448a95.99975,95.99975,0,0,0,96-96h32a47.99987,47.99987,0,0,0,48-48V240A47.99987,47.99987,0,0,0,576,192ZM96,368H64V240H96Zm400,48a48.14061,48.14061,0,0,1-48,48H192a48.14061,48.14061,0,0,1-48-48V192a47.99987,47.99987,0,0,1,48-48H448a47.99987,47.99987,0,0,1,48,48Zm80-48H544V240h32ZM240,208a48,48,0,1,0,48,48A47.99612,47.99612,0,0,0,240,208Zm160,0a48,48,0,1,0,48,48A47.99612,47.99612,0,0,0,400,208ZM384,408h64V360H384Zm-96,0h64V360H288Z"></path>
          </svg>
        </button>
      </Flip>
          <Footer style={{ textAlign: 'center'}}>
          ©2021 Donaciones - Todos los derechos reservados
          </Footer>
        </Layout>
      </Layout>
    </>
  )
}

export default PrincipalLayout;



