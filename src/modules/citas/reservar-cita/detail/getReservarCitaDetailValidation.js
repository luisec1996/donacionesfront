export const getReservarCitaDetailValidation = () => {
    return {
        form: {
            ID_ESPECIALIDAD: [{required: true, message: 'Especialidad es Requerido' }],
            ID_PERSONA: [{required: true, message: 'Médico es Requerido' }],
            DIA_SEMANA: [{required: true, message: 'Día de la semana es Requerido' }],
            FECHA: [{required: true, message: 'La Fecha es Requerido' }],
            HORA: [{required: true, message: 'La Hora es Requerido' }],
            NRO_DOCUMENTO_SEARCH: [{required: true, message: 'El DNI del Paciente es Requerido' }],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
};