import { Col, Row } from 'antd';
import React from 'react';
import { PAGE_MODE,DATE_FORMAT } from '../../../../constants/constants';
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from "../../../../framework/pages/BasePage";
import { UserUtil } from '../../../../util/UserUtil';
import { getReservarCitaDetailMode } from './getReservarCitaDetailMode';
import { getReservarCitaDetailValidation } from './getReservarCitaDetailValidation';

export class ReservarCitaDetailPage extends BasePage {
    createPageProperties = (pageApi) => {
        return {
            title: 'Reservar Cita',
            validation: getReservarCitaDetailValidation,
            mode: getReservarCitaDetailMode
        };
    }
    renderPage(pageApi) {
        const getForm = () => pageApi.getComponent('form');

        const formConfig = {
            autoLoad: true,
            load: {
                queryId: 88,
                params: {ID_CITA: pageApi.getParamId(), ES_PACIENTE: 1, USUARIO: UserUtil.getUserId()}
            },
            save:{
                queryId: 86,
                postLink: (resp, values) => '/citas/reservar-cita/reservar-cita-admin',
                params: {USUARIO: UserUtil.getUserId()}
            },
            update:{
                queryId: 87,
                postLink: (resp, values) => '/citas/reservar-cita/reservar-cita-admin',
                params: {USUARIO: UserUtil.getUserId()}
            },
            fields:[
                { type: 'divider', label: 'Datos de la Cita'},
                { type:'select', name:'ID_ESPECIALIDAD', label:'Especialidad', column: 6,load: {queryId: 71}, search: true, allowClear: true,},
                { type:'select', name:'ID_PERSONA', label:'Médico', column: 6,load: {queryId: 76}, search: true, allowClear: true,
                    parents: [
                        {name:'ID_ESPECIALIDAD', paramName: 'ID_ESPECIALIDAD', required: true}                    
                    ] 
                },
                { type:'select', name:'DIA_SEMANA', label:'Día Semana', column: 4, load: {queryId: 83}, search: true, allowClear: true,
                    parents: [
                        {name:'ID_PERSONA', paramName: 'ID_PERSONA', required: true}                   
                    ] 
                },
                { type:'select', name:'FECHA', label:'Fecha Consulta', load: {queryId: 84}, search: true, allowClear: true,  column: 4,
                    parents: [
                        {name:'ID_PERSONA', paramName: 'ID_PERSONA', required: true},
                        {name:'DIA_SEMANA', paramName: 'DIA_SEMANA', required: true}
                    ] 
                },
                
                { type:'select', name:'ID_REL_HORARIO_PERSONA', label:'Hora Consulta', column: 4,load: {queryId: 85}, search: true, allowClear: true,
                    parents: [
                        {name:'ID_PERSONA', paramName: 'ID_PERSONA', required: true},
                        {name:'FECHA', paramName: 'FECHA', required: true}
                    ]
                },
                { label:'Hora Consulta', name: 'HoraName', column: 4 },
                { type: 'divider', label: 'Datos del Paciente'},
                {type: 'search', name: 'NRO_DOCUMENTO_SEARCH', label: 'Ingrese DNI', maxLength:8, column: 12,
                    load: {
                    queryId: 58, 
                    params: () => {
                        return {...getForm().getData(), ID_TIPO_USUARIO: 2};
                    },
                    fnOk: (resp)=>{
                        let data = resp.dataObject;
                        if (data){
                            getForm().setData({
                                    ID_PACIENTE: data.ID_PERSONA,
                                    ID_TIPO_USUARIO:data.ID_TIPO_USUARIO,
                                    NOMBRES: data.NRO_DOCUMENTO,
                                    APELLIDO_PATERNO: data.APELLIDO_PATERNO,
                                    APELLIDO_MATERNO: data.APELLIDO_MATERNO,
                                    EMAIL: data.EMAIL,
                                    TELEFONO:data.TELEFONO,
                                    FECHA_NACIMIENTO: data.FECHA_NACIMIENTO,
                                    SEXO: data.SEXO
                                });
                        }
                        else{
                            alert("El DNI ingresado no existe.")
                            getForm().setData({
                                ID_PERSONA: '',
                                NOMBRES: '',
                                APELLIDO_PATERNO: '',
                                APELLIDO_MATERNO: '',
                                EMAIL: '',
                                TELEFONO: '',
                                FECHA_NACIMIENTO: '',
                                SEXO: ''
                            });
                         }
                        }
                    }
                },
                { name: 'ID_PACIENTE', type:'hidden'},
                { name: 'NOMBRES', label: 'Nombres: ', column: 6, readOnly:true },
                { name: 'APELLIDO_PATERNO', label: 'Ap. Paterno: ', column: 6, readOnly:true },
                { name: 'APELLIDO_MATERNO', label: 'Ap. Materno: ', column: 6, readOnly:true },
                { name: 'EMAIL', label: 'Email: ', column: 6, readOnly:true },
                { type: 'date', name:'FECHA_NACIMIENTO', label:'Fecha Nacimiento', column: 6, readOnly:true},
                { type:'select', name:'SEXO', label:'Sexo', column: 6,load: {queryId: 56}, readOnly:true},
                { name:'TELEFONO', label:'Teléfono / Celular', column: 6, readOnly:true},
                { type: 'textarea', name:'MOTIVO', label:'Motivo', column: 12}
            ],
            onComponentLoad : (formApi, pageApi) =>{
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)    
                    formApi.load();
            }
        };

    
        const barButtonconfig = {
            fields:[
                {...BUTTON_DEFAULT.RETURN, link: '/citas/reservar-cita/reservar-cita-admin'},
                {...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save()},
                {...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update()},
            ]
        }

        return (
            <>
                <FormFw
                        name="form"
                        getParentApi={pageApi.getApi}
                        config={formConfig}
                />
                <BarButtonFw  
                        name="bar"
                        getParentApi={pageApi.getApi}
                        config={barButtonconfig}
                />
            </>
        )

    }
}