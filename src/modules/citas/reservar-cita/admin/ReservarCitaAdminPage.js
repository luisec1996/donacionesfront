import React from 'react';
import { DATE_FORMAT } from '../../../../constants/constants';
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw, { COLUMN_DEFAULT } from '../../../../framework/grid/GridFw';
import { BasePage } from "../../../../framework/pages/BasePage";
import { UserUtil } from '../../../../util/UserUtil';

export class ReservarCitaAdminPage extends BasePage {
    createPageProperties = (pageApi) => {
        return {
            title: 'Reserva de Citas',
            //icon: 'icon-menu',
            barHeader: {
                fields: [
                    { ...BUTTON_DEFAULT.NEW, label: 'Nueva Reserva', link: '/citas/reservar-cita/reservar-cita/new' },
                ]
            }
        };
    };

    renderPage(pageApi) {
        const getGrid = () => pageApi.getComponent('grid');
        const getForm = () => pageApi.getComponent('form'); 
        const formConfig = {
            title: 'Filtros de Búsqueda',
            fields: [
                { type:'select', name:'ID_ESPECIALIDAD', label:'Especialidad', column: 4,load: {queryId: 71}, search: true, allowClear: true,},
                { type:'select', name:'ID_PERSONA', label:'Médico', column: 4,load: {queryId: 76}, search: true, allowClear: true,
                    parents: [
                        {name:'ID_ESPECIALIDAD', paramName: 'ID_ESPECIALIDAD', required: true}                    
                    ] 
                },
                { type: 'date', name:'FECHA', label:'Fecha Consulta', column: 4, format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD},
            ],
        };

        const barButtonconfig = {
            fields: [
                { ...BUTTON_DEFAULT.SEARCH, onClick: () => getGrid().load({ params: getForm().getData() }) },
                { ...BUTTON_DEFAULT.CLEAR, onClick: () => getForm().reset() }
            ]
        };

        const gridConfig = {
            showIndex: true,
            autoLoad: true,
            pagination: {
                server: true,
                showSizeChanger: true
            },
            scroll: { x: 'max-content' },
            load: {
                queryId: 89,
                params: { ES_PACIENTE: 1, USUARIO: UserUtil.getUserId()},
                hideMessage: true, 
                hideMessageError: true
            },
            fields: [
                { name: 'MEDICO', label: 'Médico' },
                { name: 'ESPECIALIDAD', label: 'Especialidad' , },
                { type: 'date', name: 'FECHA', label: 'Fecha Consulta', format: DATE_FORMAT.DDMMYYYY },
                { name: 'HORA', label: 'Hora Consulta' },
                { name: 'PACIENTE', label: 'Paciente'},
                {...COLUMN_DEFAULT.VIEW, colSpan: 3, label: 'Opciones', link: (value, row, index) => `/citas/reservar-cita/reservar-cita/${row.ID_CITA}`},
                {...COLUMN_DEFAULT.EDIT, colSpan: 0, link: (value, row, index) => `/citas/reservar-cita/reservar-cita/${row.ID_CITA}/update`},
                {...COLUMN_DEFAULT.DELETE, colSpan: 0,
                    process: {
                        fnOk: (result) => getGrid().load({ params: getForm().getData() }),
                        filter: (value, row, index) => ({ queryId: 82, params: { ID_CITA: row.ID_CITA } })
                    }
                },

            ]
        };

        return (
            <>
                <FormFw
                    name="form"
                    layout='vertical'
                    getParentApi={pageApi.getApi}
                    config={formConfig} />
                    <br />

                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={barButtonconfig} />
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={gridConfig} />
            </>
        );

    }
}
