import React from 'react'
import { Route, Switch } from 'react-router-dom'
import { ReservarCitaAdminPage } from './admin/ReservarCitaAdminPage';
import { ReservarCitaDetailPage } from './detail/ReservarCitaDetailPage';

const ReservarCitaRoutes = () => {
    return (
        <Switch>
            <Route exact path="/citas/reservar-cita/reservar-cita-admin" component={ReservarCitaAdminPage} />
            <Route path="/citas/reservar-cita/reservar-cita" component={ReservarCitaDetailPage} />
            <Route path="/citas/reservar-cita/reservar-cita/:id" component={ReservarCitaDetailPage} />
        </Switch>
    )
}

export default ReservarCitaRoutes
