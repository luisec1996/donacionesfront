export const getGenerarCitaDetailValidation = () => {
    return {
        form: {
                ID_ESPECIALIDAD: [{required: true, message: 'Especialidad es Requerido' }],
                DOCTOR: [{required: true, message: 'Médico es Requerido' }],
                // NOMBRES: [{required: true, message: 'Paciente es Requerido' }],
                FECHA: [{required: true, message: 'La Fecha es Requerido' }],
                HoraName: [{required: true, message: 'La Hora es Requerido' }],
                NRO_DOCUMENTO_SEARCH: [{required: true, message: 'El DNI del Paciente es Requerido' }],
                MOTIVO: [{required: true, message: 'Motivo es Requerido' }],
                ID_TIPO_DOCUMENTO: [{required: true, message: 'Tipo de Documento es Requerido' }],
                ID_CONDICION_PAGO: [{required: true, message: 'Condición de Pago es Requerido' }],
                ID_MONEDA: [{required: true, message: 'Moneda es Requerido' }],
                ID_SERVICIO: [{required: true, message: 'Servicio Requerido' }],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
};