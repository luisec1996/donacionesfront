import { MODE_SHORTCUT } from "../../../../constants/constants"

export const getGenerarCitaDetailMode = () => {
    return {
        new: {
            // form:{
            //     hide: ['IGV']
            // },
            bar: {
                show: ['save', 'return']
                
            }
        },
        update:{
            form: {
                read: ['ID_ESPECIALIDAD', 'ID_PERSONA', 'FECHA', 'HoraName', 'DIA_SEMANA'],
                hide: ['ID_REL_HORARIO_PERSONA']
            },
            bar: {
                show: ['update', 'return'],
                hide: ['bar2']
            },
            bar2: {
                hide: ['btnAsociarReserva']
            }
        },
        read:{
            form: {
                read: [MODE_SHORTCUT.ALL],
                hide: ['ID_REL_HORARIO_PERSONA']
            },
            bar: {
                show: ['return'],
            },
            bar2: {
                hide: ['btnAsociarReserva']
            }
        }
    }
};