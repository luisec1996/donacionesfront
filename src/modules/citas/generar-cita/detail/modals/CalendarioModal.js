import React from 'react';
import { PAGE_MODE } from '../../../../../constants/constants';
import FormFw from '../../../../../framework/form/FormFw';
import { BasePage } from '../../../../../framework/pages/BasePage';

export class CalendarioModal extends BasePage {


    createPageProperties = (pageApi) => {
        return {
            title: 'Horarios Disponibles',
        };
    }

    renderPage(pageApi) {
        const getForm = () => pageApi.getComponent('calendarForm');
        let lista = [];
        const formConfig = {
            autoLoad: true,
            title: 'Seleccione Hora de Consulta',
            load: {
                queryId: 91,
                params: () => {
                    return {ID_ESPECIALIDAD: pageApi.name.mapFieldApi.ID_ESPECIALIDAD.value}
                },
                fnOk: (resp) => {
                    JSON.parse(resp?.dataObject?.calendario).map( x => {
                        let obj = { title: x.title, start: new Date(x.start), end: new Date(x.end), hexColor: x.hexColor, id: x.ID_REL_HORARIO_PERSONA, dia_semana: x.DIA_SEMANA, fecha: x.FECHA, hora: x.HORA, id_persona: x.ID_PERSONA}
                        lista = [...lista, obj];
                    })
                    getForm().config.fields[0].value =  lista;
                    getForm().setFieldData('calendario',  lista);
                }
            },
            fields:[
                { type: 'calendar', column: 12, name: 'calendario', value: lista, onClick: ( event ) => {
                        let obj = {ID_REL_HORARIO_PERSONA: event.id, DIA_SEMANA: event.dia_semana, FECHA: event.fecha, HoraName: event.hora, ID_PERSONA: event.id_persona, DOCTOR: event.title }
                        pageApi.getEvent('afterAddEvent')(obj);
                        pageApi.hideThisModal();
                    }
                },
            ],
            onComponentLoad : (formApi, pageApi) =>{
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)    
                    formApi.load();
            }
        };

        return (
            <>
                <FormFw
                    name="calendarForm"
                    getParentApi={pageApi.getApi}
                    config={formConfig}
                />
            </>
        )

    }

}