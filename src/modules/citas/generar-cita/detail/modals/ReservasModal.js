import React from 'react';
import { DATE_FORMAT } from '../../../../../constants/constants';
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../../framework/bar-button/BarButtonFw';
import FormGridFw, { FORM_GRID_MODE } from '../../../../../framework/form-grid/FormGridFw';
import FormFw from '../../../../../framework/form/FormFw';
import { SELECTION_TYPE } from '../../../../../framework/grid/GridFw';
import { BasePage } from '../../../../../framework/pages/BasePage';
import { MessageUtil } from '../../../../../util/MessageUtil';

export class ReservasModal extends BasePage {
    
    
    createPageProperties = (pageApi) => {
        return {
            title: 'Busqueda de Reservas',
        };
    }
    
    renderPage(pageApi) {
        
        const formGridConfig = {
            mode: FORM_GRID_MODE.FORM_SEARCH_ADD,
            afterAddEvent: (data) => {
                if(data){
                    pageApi.getEvent('afterAddEvent')(data);
                    pageApi.hideThisModal();
                }
                else{
                    return MessageUtil('warning','Alerta!','Debe Seleccionar Una Reserva!');
                }
            },
            form: {
                fields:[
                    { name:'NRO_DOCUMENTO', label:'Nro. Documento', column: 6, allowClear: true},
                ]
            },
            grid: {
                title: 'Seleccione Reserva',
                rowId: 'ID_CITA',
                showIndex: false,
                autoLoad: true,
                selection: {type: SELECTION_TYPE.SINGLE},
                load: {
                    queryId: 92,
                },
                scroll: {x: true},
                fields:[
                    //{name:'id_doc', label:'Id'},
                    {name:'ID_CITA', label:'Cita'},
                    {name:'PACIENTE', label:'Paciente'},
                    {name:'NRO_DOCUMENTO_SEARCH', label: 'Nro. Documento'},
                    {type : 'date', name:'FECHA', label: 'Fecha', format: DATE_FORMAT.DDMMYYYY},
                    {name:'HORA', label: 'Hora'}
                ]
            }
        };  
    
        return (
            <>
                <FormGridFw 
                    name="reservasGrid" 
                    getParentApi={pageApi.getApi}
                    config={formGridConfig}
                />
            </>
        )

    }
    
}