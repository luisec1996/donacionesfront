import { Col, Row } from 'antd';
import React from 'react';
import { PAGE_MODE,DATE_FORMAT } from '../../../../constants/constants';
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from "../../../../framework/pages/BasePage";
import { MessageUtil } from '../../../../util/MessageUtil';
import { UserUtil } from '../../../../util/UserUtil';
import { getGenerarCitaDetailMode } from './getGenerarCitaDetailMode';
import { getGenerarCitaDetailValidation } from './getGenerarCitaDetailValidation';
import { CalendarioModal } from './modals/CalendarioModal';
import { ReservasModal } from './modals/ReservasModal';

export class GenerarCitaDetailPage extends BasePage {
    createPageProperties = (pageApi) => {
        return {
            title: 'Generar Cita',
            validation: getGenerarCitaDetailValidation,
            mode: getGenerarCitaDetailMode
        };
    }
    renderPage(pageApi) {
        const getForm = () => pageApi.getComponent('form');
    
        //getForm()?.getConfig()?.fields[25].setVisible(false);
        
        const calendarioConfig = {
            events: {
                afterAddEvent: (data) => {
                    getForm().setData(data)
                }
            }
        }
    
        const CalendarioModalConfig = (data) => {
            return{
                width: 'full',
                maskClosable: false,
                component: <CalendarioModal name={data} getParentApi={pageApi.getApi} config={calendarioConfig} />
            }
            
        };

        const reservasConfig = {
            events: {
                afterAddEvent : (data) => {
                    getForm().setData(data[0]);   
                }
            }
        }

        const ReservasModalConfig = {
            width: 900,
            maskClosable: false,
            component: <ReservasModal name="reservasModal" getParentApi={pageApi.getApi} config={reservasConfig} />
        };

        const cambiarPrecios = () => {
            let COSTO = parseFloat(getForm()?.getFieldApi('ID_SERVICIO')?.getSelectedItem()?.COSTO).toFixed(2)
            let TIPO_CAMBIO = parseFloat(getForm()?.getData()?.TIPO_CAMBIO).toFixed(2);
            let ID_MONEDA = getForm()?.getData()?.ID_MONEDA;
            let ID_TIPO_DOCUMENTO = getForm()?.getData()?.ID_TIPO_DOCUMENTO;

            if(ID_TIPO_DOCUMENTO == 1){
                if(ID_MONEDA == 1){
                    if(COSTO > 0){
                        getForm().setFieldData('COSTO', COSTO );
                        getForm().setFieldData('IGV', '0.00');
                        getForm().setFieldData('TOTAL', COSTO);
                    }
                }
                else{
                    if(COSTO > 0){
                        getForm().setFieldData('COSTO', parseFloat(COSTO / TIPO_CAMBIO).toFixed(2));
                        getForm().setFieldData('IGV', '0.00');
                        getForm().setFieldData('TOTAL', parseFloat((COSTO / TIPO_CAMBIO)).toFixed(2));
                    }
                }
            }
            else{
                if(ID_MONEDA == 1){
                    if(COSTO > 0){
                        getForm().setFieldData('COSTO', COSTO );
                        getForm().setFieldData('IGV', parseFloat(COSTO * 0.18).toFixed(2));
                        getForm().setFieldData('TOTAL', parseFloat(COSTO * 1.18).toFixed(2));
                    }
                }
                else{
                    if(COSTO > 0){
                        getForm().setFieldData('COSTO', parseFloat(COSTO / TIPO_CAMBIO).toFixed(2));
                        getForm().setFieldData('IGV',parseFloat((COSTO / TIPO_CAMBIO) * 0.18).toFixed(2));
                        getForm().setFieldData('TOTAL', parseFloat((COSTO / TIPO_CAMBIO) * 1.18).toFixed(2));
                    }
                }
            }
        }
        const formConfig = {
            autoLoad: true,
            load: {
                queryId: 95,
                params: {ID_PROCESO: pageApi.getParamId() || 0},
                fnOk: () => {
                }
            },
            save:{
                queryId: 93,
                postLink: (resp, values) => '/citas/generar-cita/generar-cita-admin',
                params: {USUARIO: UserUtil.getUserId()}
            },
            fields:[
                { type: 'divider', label: 'Datos de la Cita'},
                { type: 'hidden', name:'ID_CITA'},
                { type: 'hidden', name:'TIENE_RESERVA'},
                { type:'select', name:'ID_ESPECIALIDAD', label:'Especialidad', column: 4,load: {queryId: 71}, search: true, allowClear: true,},
                { type: 'button', column: 4, name: 'modal', onClick: (value) => {

                        if(getForm().mapFieldApi.ID_ESPECIALIDAD.value == undefined){
                            return MessageUtil('warning','Alerta!','Debe Seleccionar Especialidad');
                        }
                        pageApi.showModal(CalendarioModalConfig(getForm()))
                    } 
                },
                { type:'decimal', name:'TIPO_CAMBIO', label: 'Tipo de Cambio' , column: 4, readOnly: true},
                { type:'hidden', name:'ID_PERSONA'},
                { name:'DOCTOR', label: 'Doctor', column: 4, readOnly: true},
                { type:'hidden', name:'DIA_SEMANA' },
                { type: 'date', name:'FECHA', label:'Fecha Consulta', allowClear: true,  column: 4, readOnly: true, format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD},
                { type:'hidden', name:'ID_REL_HORARIO_PERSONA'},
                { label:'Hora Consulta', name: 'HoraName', column: 4, readOnly: true },
                { type: 'divider', label: 'Datos del Paciente'},
                { type: 'search', name: 'NRO_DOCUMENTO_SEARCH', label: 'Ingrese DNI', maxLength:8, column: 4,
                    load: {
                    queryId: 58, 
                    params: () => {
                        return {...getForm().getData(), ID_TIPO_USUARIO: 2};
                    },
                    fnOk: (resp)=>{
                        let data = resp.dataObject;
                        if (data){
                            getForm().setData({
                                    ID_PACIENTE: data.ID_PERSONA,
                                    NOMBRES: data.NOMBRES,
                                    APELLIDO_PATERNO: data.APELLIDO_PATERNO,
                                    APELLIDO_MATERNO: data.APELLIDO_MATERNO,
                                    EMAIL: data.EMAIL,
                                    TELEFONO:data.TELEFONO,
                                    FECHA_NACIMIENTO: data.FECHA_NACIMIENTO,
                                    SEXO: data.SEXO
                                });
                        }
                        else{
                            alert("El DNI ingresado no existe.")
                            getForm().setData({
                                ID_PERSONA: '',
                                NOMBRES: '',
                                APELLIDO_PATERNO: '',
                                APELLIDO_MATERNO: '',
                                EMAIL: '',
                                TELEFONO: '',
                                FECHA_NACIMIENTO: '',
                                SEXO: ''
                            });
                         }
                        }
                    }
                },
                { name: 'ID_PACIENTE', type:'hidden'},
                { name: 'NOMBRES', label: 'Nombres: ', column: 4, readOnly:true },
                { name: 'APELLIDO_PATERNO', label: 'Ap. Paterno: ', column: 4, readOnly:true },
                { name: 'APELLIDO_MATERNO', label: 'Ap. Materno: ', column: 4, readOnly:true },
                { name: 'EMAIL', label: 'Email: ', column: 4, readOnly:true },
                { type: 'date', name:'FECHA_NACIMIENTO', label:'Fecha Nacimiento', column: 4, readOnly:true},
                { type: 'select', name:'SEXO', label:'Sexo', column: 4,load: {queryId: 56}, readOnly:true},
                { name: 'TELEFONO', label:'Teléfono / Celular', column: 4, readOnly:true},
                { type: 'textarea', name:'MOTIVO', label:'Motivo', column: 6},
                { type: 'divider', label: 'Datos de la Facturación'},
                { type: 'select', name:'ID_MONEDA', label: 'Moneda' , column: 4, load: { queryId: 100}, value : 1,
                    onChange:(value, values, formApi) => {
                        cambiarPrecios();
                    }
                },
                { type: 'select', name:'ID_TIPO_DOCUMENTO', label: 'Tipo de Documento' , column: 4, load: { queryId: 97}, value: 1,
                    onChange:(value, values, formApi) => {
                        cambiarPrecios();
                    }
                },
                { type: 'select', name:'ID_CONDICION_PAGO', label: 'Condición de Pago' , column: 4, load: { queryId: 98}, value: 1},
                { type:'select', name:'ID_SERVICIO', labelName:'DES_SERVICIO', label:'Servicio', column: 4,load: {queryId: 96},
                    parents: [
                        { name: 'ID_PERSONA', paramName: 'ID_PERSONA', required: true }
                    ],
                    onChange: (value, values, formApi) => {
                        cambiarPrecios();
                    }
                },
                {type:'decimal', name:'COSTO', label: 'Precio' ,column: 4,
                    onChange: (value, values, formApi) => {
                        cambiarPrecios();
                    }
                },
                {type:'decimal', name:'IGV', label: 'IGV' ,column: 4, readOnly: true},
                {type:'decimal', name:'TOTAL', label: 'Total' ,column: 4, readOnly: true},
                
            ],
            onComponentLoad : (formApi, pageApi) =>{
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)    
                    formApi.load();
            }
        };

    
        const barButtonconfig = {
            fields:[
                {...BUTTON_DEFAULT.RETURN, link: '/citas/generar-cita/generar-cita-admin'},
                {...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save()},
            ]
        }

        const bar_asociar_reserva = {
            fields: [
                {...BUTTON_DEFAULT.ADD, name: 'btnAsociarReserva' ,label: 'Asociar Reserva', onClick: () => {pageApi.showModal(ReservasModalConfig)}},
            ],
        }

        return (
            <>
                <BarButtonFw  
                        name="bar2"
                        getParentApi={pageApi.getApi}
                        config={bar_asociar_reserva}
                />

                <FormFw
                        name="form"
                        getParentApi={pageApi.getApi}
                        config={formConfig}
                />
                <BarButtonFw  
                        name="bar"
                        getParentApi={pageApi.getApi}
                        config={barButtonconfig}
                />
            </>
        )

    }
}