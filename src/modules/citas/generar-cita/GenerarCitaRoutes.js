import React from 'react'
import { Route, Switch } from 'react-router-dom'
import { GenerarCitaAdminPage } from './admin/GenerarCitaAdminPage';
import { GenerarCitaDetailPage } from './detail/GenerarCitaDetailPage';

const GenerarCitaRoutes = () => {
    return (
        <Switch>
            <Route exact path="/citas/generar-cita/generar-cita-admin" component={GenerarCitaAdminPage} />
            <Route path="/citas/generar-cita/generar-cita" component={GenerarCitaDetailPage} />
            <Route path="/citas/generar-cita/generar-cita/:id" component={GenerarCitaDetailPage} />
        </Switch>
    )
}

export default GenerarCitaRoutes
