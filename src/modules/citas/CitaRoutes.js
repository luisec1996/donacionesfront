import React, {lazy} from 'react'
import {
    Switch,
    Route
  } from "react-router-dom"


const ReservarCitaRoutes = lazy(() => import('./reservar-cita/ReservarCitaRoutes'));
const GenerarCitaRoutes = lazy(() => import('./generar-cita/GenerarCitaRoutes'));
const FacturacionRoutes = lazy(() => import('./facturacion/FacturacionRoutes'));

const CitaRoutes = () => {
    return (
        <Switch>
            <Route path="/citas/reservar-cita" component={ReservarCitaRoutes} />
            <Route path="/citas/generar-cita" component={GenerarCitaRoutes} />
            <Route path="/citas/facturacion" component={FacturacionRoutes} />
        </Switch>
    )
}
export default CitaRoutes; 

