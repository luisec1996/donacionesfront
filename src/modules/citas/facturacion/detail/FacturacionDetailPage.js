import { Col, Row } from 'antd';
import React from 'react';
import { PAGE_MODE,DATE_FORMAT } from '../../../../constants/constants';
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw, { COLUMN_DEFAULT } from '../../../../framework/grid/GridFw';
import { BasePage } from "../../../../framework/pages/BasePage";
import { MessageUtil } from '../../../../util/MessageUtil';
import { UserUtil } from '../../../../util/UserUtil';
import { getFacturacionDetailMode } from './getFacturacionDetailMode';
import { getFacturacionDetailValidation } from './getFacturacionDetailValidation';
import { MedicamentosModal, ReservasModal } from './modals/MedicamentosModal';

export class FacturacionDetailPage extends BasePage {
    createPageProperties = (pageApi) => {
        return {
            title: 'Datos de Facturación',
            validation: getFacturacionDetailValidation,
            mode: getFacturacionDetailMode
        };
    }
    renderPage(pageApi) {
        const getForm = () => pageApi.getComponent('form');
        const getGrid = () => pageApi.getComponent('itemsGrid');
        const getFormCalculos = () => pageApi.getComponent('formCalculos');

        const formConfig = {
            autoLoad: true,
            load: {
                queryId: 104,
                params: {ID_DOCUMENTO: pageApi.getParamId() || 0},
                fnOk: (resp) => {
                    getFormCalculos().setData({...resp?.dataObject})
                    getGrid().load({ID_DOCUMENTO: pageApi.getParamId()})
                }
            },
            save:{
                queryId: 102,
                postLink: (resp, values) => '/citas/facturacion/facturacion-admin',
                params: () => { return{...getFormCalculos()?.getData(), USUARIO: UserUtil.getUserId() }},
                fnOk: (resp) => {
                    getGrid().save({params:{...getForm().getData(), ID_DOCUMENTO: resp?.dataObject?.ID_DOCUMENTO}})
                }
            },
            update:{
                queryId: 103,
                postLink: (resp, values) => '/citas/facturacion/facturacion-admin',
                params: { ...getFormCalculos()?.getData(), USUARIO: UserUtil.getUserId()},
                fnOk: (resp) => {
                    getGrid().save({params:{...getForm().getData(), ID_DOCUMENTO: pageApi.getParamId()}})
                }
            },
            fields:[
                { type: 'select', name:'ID_TIPO_DOCUMENTO', label: 'Tipo de Documento' , column: 4, load: { queryId: 97}, value: 1,
                    onChange:(value, values, formApi) => {
                        calculateTotal();
                        let sub_total = getFormCalculos()?.getData().SUB_TOTAL;
                        if(value == 1){
                            getFormCalculos()?.setFieldData('IGV','0.00');
                            getFormCalculos()?.setFieldData('TOTAL_APAGAR', parseFloat(sub_total).toFixed(2));
                        }
                        else{
                            if(parseFloat(sub_total) > 0){
                                getFormCalculos()?.setFieldData('IGV',parseFloat(sub_total * 0.18).toFixed(2));
                                getFormCalculos()?.setFieldData('TOTAL_APAGAR', parseFloat(sub_total * 1.18).toFixed(2));
                            }
                        }
                    }
                },
                { name: 'SERIE_DOCUMENTO', label: 'Nro. Documento', column: 4, readOnly: true, value: 'Autogenerado' },
                { type: 'select', name:'ID_MONEDA', label: 'Moneda' , column: 4, load: { queryId: 100}, value: 1, },
                { type:'decimal', name:'TIPO_CAMBIO', label: 'Tipo de Cambio' ,column: 4, readOnly: true},
                { type: 'select', name:'ID_CONDICION_PAGO', label: 'Condición de Pago' , column: 4, load: { queryId: 98}, value: 1,},
                { type: 'search', name: 'NRO_DOCUMENTO_SEARCH', label: 'Ingrese DNI', maxLength:8, column: 6,
                    load: {
                    queryId: 58, 
                    params: () => {
                        return {...getForm().getData(), ID_TIPO_USUARIO: 2};
                    },
                    fnOk: (resp)=>{
                        let data = resp.dataObject;
                        if (data){
                            getForm().setData({
                                ID_PACIENTE: data.ID_PERSONA,
                                NOMBRES: data.NOMBRES +' '+ data.APELLIDO_PATERNO +' '+ data.APELLIDO_MATERNO
                            });
                        }
                        else{
                            getForm().setData({
                                NRO_DOCUMENTO_SEARCH:'',
                                ID_PERSONA: '',
                                NOMBRES: '',
                            });
                            return MessageUtil('error','',"El DNI ingresado no existe.")
                         }
                        }
                    }
                },
                { name: 'ID_PACIENTE', type:'hidden' },
                { name: 'NOMBRES', label: 'Datos Cliente', column: 6, readOnly: true },
            ],
            onComponentLoad : (formApi, pageApi) =>{
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)    
                    formApi.load();
            }
        };


        const fnDelete = (value, row, index) => {
            getGrid().deleteItem(row.key);      
        }
    
        const medicamentosConfig = {
            events: {
                afterAddEvent : (data) => {
                    getGrid().addItem(data);
                }
            }
        }
    
        const MedicamentosModalConfig = (data) =>  {
            return {
                width: 1600,
                maskClosable: false,
                component: <MedicamentosModal name={data}  getParentApi={pageApi.getApi} config={medicamentosConfig}  />
            }
        };
    
    
        const barHeaderConfig = {
            fields:[
                {...BUTTON_DEFAULT.NEW , label:'Agregar Items', onClick: ()=>{  pageApi.showModal(MedicamentosModalConfig(getForm().getData())); }}
            ]
        }
    
        const calculateTotal = () => {
            let items = getGrid()?.getData();
            let ID_TIPO_DOCUMENTO = getForm()?.getData()?.ID_TIPO_DOCUMENTO
            if (items) {
                let total = 0;
                items.map(item => {
                    total = total + parseFloat(item.IMPORTE);
                });

                let sub_total = parseFloat(total).toFixed(2);
    
                getFormCalculos().setFieldData('SUB_TOTAL', sub_total);

                if(ID_TIPO_DOCUMENTO == 1){
                    getFormCalculos().setFieldData('IGV', '0.00');
                    getFormCalculos().setFieldData('TOTAL_APAGAR', sub_total);
                }
                else{
                    getFormCalculos().setFieldData('IGV', parseFloat(sub_total * 0.18).toFixed(2));
                    getFormCalculos().setFieldData('TOTAL_APAGAR', parseFloat(sub_total * 1.18).toFixed(2));
                }
                
            }
        }
    
    
        const gridConfig = {
            title: 'Items',
            rowId: 'ID_DETALLE_DOCUMENTO',
            showIndex: true,
            pagination: true,
            autoLoad: true,
            addItem: {
                fnAfter: (row) => {
                    calculateTotal();
                }
            },
            updateItem: {
                fnAfter: (row) => {
                    calculateTotal();
                }
            },
            load: {
                validation: () => pageApi.getParamId() != null, //solo se dispara si el valor es distinto de nulo
                queryId: 106,
                params: { ID_DOCUMENTO: pageApi.getParamId() },
                fnOk: () => {
                    calculateTotal();
                },
                hideMessage: true, 
                hideMessageError: true
            },
            save: {
                queryId: 105,
                params: { ID_DOCUMENTO: pageApi.getParamId() },
                hideMessage: true, 
                hideMessageError: true
            },
            fields: [
                { name: 'MEDICAMENTO', label: 'Item', editable: false },
                //{ name:'PRESENTACION', label: 'Presentación', editable: false},
                //{ type: 'number', name:'STOCK', label: 'Stock', editable: false},
                { type: 'number', name:'CANTIDAD', label: 'Cantidad', editable: true,
                    onChange: (value, values, formApi) => { //el formApi trae la fila

                        let CANTIDAD = parseInt(values['CANTIDAD']);
                        let PRECIO = parseFloat(values['PRECIO']).toFixed(2);

                        // if(CANTIDAD > parseInt(values['STOCK'])){
                        //     formApi.setFieldData('CANTIDAD', '');
                        //     return MessageUtil('warning','Alerta!','La Cantidad no puede ser Mayor al Stock!');
                        // }

                        formApi.setFieldData('IMPORTE', isNaN(parseFloat(CANTIDAD * PRECIO).toFixed(2)) ? '0.00' : parseFloat(CANTIDAD * PRECIO).toFixed(2))
                        calculateTotal();
                    }
                },
                { type: 'decimal', name:'PRECIO', label: 'Precio', editable: true,
                    onChange: (value, values, formApi) => { //el formApi trae la fila

                        let CANTIDAD = parseInt(values['CANTIDAD']);
                        let PRECIO = parseFloat(values['PRECIO']).toFixed(2);

                        // if(CANTIDAD > parseInt(values['STOCK'])){
                        //     formApi.setFieldData('CANTIDAD', '');
                        //     return MessageUtil('warning','Alerta!','La Cantidad no puede ser Mayor al Stock!');
                        // }

                        formApi.setFieldData('IMPORTE', isNaN(parseFloat(CANTIDAD * PRECIO).toFixed(2)) ? '0.00' : parseFloat(CANTIDAD * PRECIO).toFixed(2))
                        calculateTotal();
                    }
                },
                { type: 'decimal', name: 'IMPORTE', label: 'Sub Total', editable: true, readOnly: true, },
                {...COLUMN_DEFAULT.DELETE, onClick: fnDelete}
            ],
            barHeader: barHeaderConfig
        };

        const formCalculosConfig = {
            fields:[
                { type: 'decimal', name:'SUB_TOTAL', label: 'Sub Total' , column: 4, readOnly: true},
                { type: 'decimal', name:'IGV', label: 'IGV' , column: 4, readOnly: true},
                { type: 'decimal', name:'TOTAL_APAGAR', label: 'Total a Pagar' , column: 4, readOnly: true},
            ]
        };

    
        const barButtonconfig = {
            fields:[
                {...BUTTON_DEFAULT.RETURN, link: '/citas/facturacion/facturacion-admin'},
                {...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save()},
                // {...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update()},
            ]
        }


        return (
            <>
                <FormFw
                        name="form"
                        getParentApi={pageApi.getApi}
                        config={formConfig}
                />

                <GridFw
                    name="itemsGrid"
                    getParentApi={pageApi.getApi}
                    config={gridConfig}
                />
                <br/>
                <FormFw
                        name="formCalculos"
                        getParentApi={pageApi.getApi}
                        config={formCalculosConfig}
                />
                
                <BarButtonFw  
                        name="bar"
                        getParentApi={pageApi.getApi}
                        config={barButtonconfig}
                />
            </>
        )

    }
}