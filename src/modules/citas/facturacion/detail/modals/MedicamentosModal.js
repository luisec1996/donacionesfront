import React from 'react';
import FormGridFw, { FORM_GRID_MODE } from '../../../../../framework/form-grid/FormGridFw';
import { SELECTION_TYPE } from '../../../../../framework/grid/GridFw';
import { BasePage } from '../../../../../framework/pages/BasePage';
import { MessageUtil } from '../../../../../util/MessageUtil';

export class MedicamentosModal extends BasePage {
    
    
    createPageProperties = (pageApi) => {
        return {
            title: 'Busqueda de Medicamentos',
        };
    }
    
    renderPage(pageApi) {
        const formGridConfig = {
            mode: FORM_GRID_MODE.FORM_SEARCH_ADD,
            afterAddEvent: (data) => {
                if(data){
                    pageApi.getEvent('afterAddEvent')(data);
                    pageApi.hideThisModal();
                }
                else{
                    return MessageUtil('warning','Alerta!','Debe Seleccionar Por lo menos un Medicamento!');
                }
            },
            form: {
                fields:[
                    { name:'MEDICAMENTO', label:'Medicamento', column: 6, allowClear: true},
                ]
            },
            grid: {
                title: 'Seleccione Reserva',
                rowId: 'ID_CITA',
                showIndex: false,
                autoLoad: true,
                selection: {type: SELECTION_TYPE.MULTIPLE},
                load: {
                    queryId: 101,
                    params: () => {
                        return {ID_MONEDA: pageApi?.name?.ID_MONEDA, TIPO_CAMBIO: pageApi?.name?.TIPO_CAMBIO}
                    }
                },
                scroll: {x: true},
                fields:[
                    // {name:'ID_MEDICAMENTO', label:'Id'},
                    {name:'MEDICAMENTO', label:'Medicamento'},
                    {name:'PRESENTACION', label: 'Presentación'},
                    {name:'STOCK', label: 'Stock'},
                    {type: 'decimal', name:'PRECIO', label: 'Precio'}
                ]
            }
        };  
    
        return (
            <>
                <FormGridFw 
                    name="reservasGrid" 
                    getParentApi={pageApi.getApi}
                    config={formGridConfig}
                />
            </>
        )

    }
    
}