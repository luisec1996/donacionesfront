export const getFacturacionDetailValidation = () => {
    return {
        form: {
                ID_ESPECIALIDAD: [{required: true, message: 'Especialidad es Requerido' }],
                DOCTOR: [{required: true, message: 'Médico es Requerido' }],
                NOMBRES: [{required: true, message: 'Paciente es Requerido' }],
                FECHA: [{required: true, message: 'La Fecha es Requerido' }],
                HoraName: [{required: true, message: 'La Hora es Requerido' }],
                NRO_DOCUMENTO_SEARCH: [{required: true, message: 'El DNI del Paciente es Requerido' }],
                MOTIVO: [{required: true, message: 'Motivo es Requerido' }],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
};