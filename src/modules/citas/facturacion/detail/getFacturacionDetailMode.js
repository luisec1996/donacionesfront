import { MODE_SHORTCUT } from "../../../../constants/constants"

export const getFacturacionDetailMode = () => {
    return {
        new: {
            // form:{
            //     hide: ['HoraName']
            // },
            bar: {
                show: ['save', 'return']
                
            }
        },
        update:{
            form: {
                read: ['ID_ESPECIALIDAD', 'ID_PERSONA', 'FECHA', 'HoraName', 'DIA_SEMANA'],
                hide: ['ID_REL_HORARIO_PERSONA']
            },
            bar: {
                show: ['update', 'return']
            }
        },
        read:{
            form: {
                read: [MODE_SHORTCUT.ALL],
                hide: ['ID_REL_HORARIO_PERSONA']
            },
            bar: {
                show: ['return']
            }
        }
    }
};