import React from 'react';
import { DATE_FORMAT } from '../../../../constants/constants';
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw, { COLUMN_DEFAULT } from '../../../../framework/grid/GridFw';
import { BasePage } from "../../../../framework/pages/BasePage";
import DocumentoFacturacion from './pdf/DocumentoFacturacion';
import { LoadingUtil } from '../../../../util/LoadingUtil';

export class FacturacionAdminPage extends BasePage {
    createPageProperties = (pageApi) => {
        return {
            title: 'Facturación',
            //icon: 'icon-menu',
            barHeader: {
                fields: [
                    { ...BUTTON_DEFAULT.NEW, label: 'Nuevo', link: '/citas/facturacion/facturacion/new' },
                ]
            }
        };
    };

    renderPage(pageApi) {
        const getGrid = () => pageApi.getComponent('grid');
        const getForm = () => pageApi.getComponent('form'); 
        const formConfig = {
            title: 'Filtros de Búsqueda',
            fields: [
                { type: 'select', name:'ID_TIPO_DOCUMENTO', label: 'Tipo de Documento' , column: 4, load: { queryId: 97}},
                { name: 'SERIE_DOCUMENTO', label:'Nro. Documento', column: 4, allowClear: true,},
                { type: 'date', name:'FECHA', label:'Fecha Emisión', column: 4, format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD},
                { name: 'PACIENTE', label: 'Cliente', column: 4, allowClear: true,},
            ],
        };

        const barButtonconfig = {
            fields: [
                { ...BUTTON_DEFAULT.SEARCH, onClick: () => getGrid().load({ params: getForm().getData() }) },
                { ...BUTTON_DEFAULT.CLEAR, onClick: () => getForm().reset() }
            ]
        };

        const gridConfig = {
            title: 'Lista de Documentos',
            showIndex: true,
            autoLoad: true,
            pagination: {
                server: true,
                showSizeChanger: true
            },
            scroll: { x: 'max-content' },
            load: {
                queryId: 99,
            },
            fields: [
                { name: 'TIPO_DOCUMENTO', label: 'Tipo de Documento' },
                { name: 'SERIE_DOCUMENTO', label: 'Nro. Documento' },
                { type: 'date', name: 'FECHA_EMISION', label: 'Fecha Emisión', format: DATE_FORMAT.DDMMYYYY },
                //{ name: 'TOTAL_APAGAR', label: 'Total' },
                { name: 'PACIENTE', label: 'Cliente'},
                {...COLUMN_DEFAULT.VIEW, colSpan: 2, label: 'Opciones', link: (value, row, index) => `/citas/facturacion/facturacion/${row.ID_DOCUMENTO}`},
                //{...COLUMN_DEFAULT.EDIT, colSpan: 0, link: (value, row, index) => `/citas/facturacion/facturacion/${row.ID_CITA}/update`},
                // {...COLUMN_DEFAULT.DELETE, colSpan: 0,
                //     process: {
                //         fnOk: (result) => getGrid().load({ params: getForm().getData() }),
                //         filter: (value, row, index) => ({ queryId: 82, params: { ID_CITA: row.ID_CITA } })
                //     }
                // },
                {...COLUMN_DEFAULT.DOWNLOAD, title: 'Descargar Documento', color: 'red', colSpan:0, onClick: (value, row, index)=> 
                    {
                        LoadingUtil.loadShow();
                        // LoadUtil.loadByQueryId({
                        //     url: EnvConstants.GET_FW_FORM_URL_LOAD_BASE(),
                        //     queryId: 445,
                        //     reqParams: {
                        //       params: {
                        //         id_doc: row.id_doc
                        //       }
                        //     },
                        //     fnOk: (resp) => {
                            setTimeout(function(){
                                DocumentoFacturacion(row, 1)
                                LoadingUtil.loadHide();
                            },2000)
                        
                                //DocumentoFacturacion(row, 1);
                                
                        //     }
                        // })
                        
                    }
                }

            ]
        };

        return (
            <>
                <FormFw
                    name="form"
                    layout='vertical'
                    getParentApi={pageApi.getApi}
                    config={formConfig} />
                    <br />

                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={barButtonconfig} />

                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={gridConfig} />
            </>
        );

    }
}
