import jsPDF from 'jspdf'
import autotable from 'jspdf-autotable'
import LOGO from '../../../../../assets/img/LOGO.png'

const DocumentoFacturacion = (props, type_return) => {

    const doc = new jsPDF();
    let header_style = (align) => { return { halign: align, valign: 'middle', fillColor: [11,24,73], textColor: [255,255,255], fontSize: 7, lineColor: [0,0,0], fontStyle: 'bold'}};
    let body_style =  (align) => { return{ halign: align, valign: 'middle', fillColor: [255,255,255], textColor: [0,0,0] , fontSize: 7, lineColor: [0,0,0]}};
    

    let sub_total = 0.00;
    let body = [];

    let id_moneda = ( props.ID_MONEDA == 1 ) ? 'S/' : '$'; 
    if(props.items != null){
      let items = JSON.parse(props.items);
      items.forEach(x => {
        sub_total = sub_total + x.IMPORTE;

        body = [...body, 
            [
                { content: x.MEDICAMENTO, styles: body_style('center')}, 
                { content: x.CANTIDAD, styles: body_style('center')},
                { content: id_moneda + ' ' + parseFloat(x.PRECIO).toFixed(2), styles: body_style('center')},
                { content: id_moneda + ' ' + parseFloat(x.IMPORTE).toFixed(2), styles: body_style('center')},
            ],
        ];
      });
    }

    // const data = props;
    // //data pdf
    
     
  //Seccion superior derecha
    //Palabra factura parte superior derecha
    if(props.ID_TIPO_DOCUMENTO == 1){
      doc.text(150, 20,'BOLETA DE VENTA');
    }
    else{
      doc.text(150, 20,'FACTURA');
    }
    
    doc.addImage(LOGO, 'JPEG', 20, 10, 60, 30)
    
    //Rectangulo serie
    doc.setDrawColor(0);
    doc.setFillColor(234,234,234);
    doc.roundedRect(125, 35, 20, 7, 1, 1, 'F'); 
    doc.setFontSize(8);
    doc.text(126,38,'Serie');
    doc.text(136,38,props.CODIGO_DOCUMENTO);
    //Rectangulo No.de factura

    doc.setDrawColor(0);
    doc.setFillColor(234,234,234);
    doc.roundedRect(150, 35, 55, 7, 1, 1, 'F'); 
    doc.setFontSize(8);
    doc.text(151,38,'N°: ');
    doc.text(171,38,props.SERIE_DOCUMENTO);
    
    //Rectangulo fecha
    doc.setDrawColor(0);
    doc.setFillColor(234,234,234);
    doc.roundedRect(125, 47,80, 10, 1, 1, 'F'); 
    doc.text(126,50,'Fecha y hora: '+ props.FECHA);
    doc.text(126,55,'Tipo de Cambio: '+ props.TIPO_CAMBIO);
    doc.setFontSize(7);
    

    doc.setFontSize(10);
    doc.setDrawColor(0);
    doc.setFillColor(234,234,234);
    doc.roundedRect(10, 47, 109, 15, 1, 1, 'F'); 
    doc.text(10,50,'Clínica Briceño.');
    doc.text(10,55,'R.U.C: 20493869311');
    doc.text(10,60,'Direccion: Jr. Jorge Chavez Nro. 525');

    doc.setDrawColor(0);
    doc.setFillColor(234,234,234);
    doc.roundedRect(10, 65, 109, 18, 1, 1, 'F'); 
    doc.text(11,70,'Nombre del cliente: '+ props.PACIENTE); 
    doc.text(11,75,'Domicilio: '+ props.DIRECCION);
    doc.text(11,80,'Moneda: '+ props.MONEDA);

    doc.autoTable({
      head: [[
        { content: 'Item', styles: header_style('center')}, 
        { content: 'Precio', styles: header_style('center')},
        { content: 'Cantidad', styles: header_style('center')},
        { content: 'Sub Total', styles: header_style('center')},
      ]],
      body,
      foot: [
        [
          { content: 'Sub Total', styles: body_style('right'), colSpan: 3},{ content: id_moneda + ' ' + parseFloat(sub_total).toFixed(2), styles: body_style('center')} 
        ],
        [
          { content: 'IGV', styles: body_style('right'), colSpan: 3}, { content: id_moneda + ' ' + ((props.ID_TIPO_DOCUMENTO == 1) ? '0.00' : parseFloat(sub_total * 0.18).toFixed(2) ), styles: body_style('center')}
        ],
        [
          { content: 'Total', styles: body_style('right'), colSpan: 3}, { content: id_moneda + ' ' +((props.ID_TIPO_DOCUMENTO == 1) ? parseFloat(sub_total).toFixed(2) : parseFloat(sub_total * 1.18).toFixed(2)), styles: body_style('center')}
        ]
      ],
      startY: 90,
      styles: {lineWidth: 0.5, cellPadding: 1.5, fontSize: 8},
      //tableLineWidth:0.5,
      //tableLineColor: [0, 0, 0],
      showHead: 'firstPage',
      pageBreak: 'auto',
      rowPageBreak: 'auto',
      // columnStyles:{
      //     0: {columnWidth:35},
      //     1: {columnWidth:175},
      //     2: {columnWidth:175},
      //     3: {columnWidth:50},
      //     4: {columnWidth:40},
      //     5: {columnWidth:40},
      // },
   });

  
      if(type_return == 1){
        if(props.ID_TIPO_DOCUMENTO == 1){
          return doc.save(`Boleta_${props.SERIE_DOCUMENTO}.pdf`);
        }
        else{
          return doc.save(`Factura_${props.SERIE_DOCUMENTO}.pdf`);
        }
      }
      else{
          return btoa(doc.output());
      }
    };

export default DocumentoFacturacion;
