import React from 'react'
import { Route, Switch } from 'react-router-dom'
import { FacturacionAdminPage } from './admin/FacturacionAdminPage';
import { FacturacionDetailPage } from './detail/FacturacionDetailPage';

const FacturacionRoutes = () => {
    return (
        <Switch>
            <Route exact path="/citas/facturacion/facturacion-admin" component={FacturacionAdminPage} />
            <Route path="/citas/facturacion/facturacion" component={FacturacionDetailPage} />
            <Route path="/citas/facturacion/facturacion/:id" component={FacturacionDetailPage} />
        </Switch>
    )
}

export default FacturacionRoutes
