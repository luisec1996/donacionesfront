import React from 'react'
import { Route, Switch } from 'react-router-dom'
import { RegistrarDonacionDineroAdminPage } from './admin/RegistrarDonacionDineroAdminPage';

const RegistrarDonacionDineroRoutes = () => {
    return (
        <Switch>
            <Route exact path="/donaciones/registrar-donacion-dinero/registrar-donacion-dinero-admin" component={RegistrarDonacionDineroAdminPage} />
        </Switch>
    )
}

export default RegistrarDonacionDineroRoutes
