import { getDefaultNormalizer } from '@testing-library/dom';
import { Col, Row } from 'antd';
import confirm from 'antd/lib/modal/confirm';
import React from 'react';
import { DATE_FORMAT, PAGE_MODE } from '../../../../constants/constants';
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormGridFw, { FORM_GRID_MODE } from '../../../../framework/form-grid/FormGridFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw, { COLUMN_DEFAULT } from '../../../../framework/grid/GridFw';
import { BasePage } from "../../../../framework/pages/BasePage";
import { MessageUtil } from '../../../../util/MessageUtil';
import { UserUtil } from '../../../../util/UserUtil';
import { getRegistrarDonacionMode } from './getRegistrarDonacionMode';
import { getRegistrarDonacionValidation } from './getRegistrarDonacionValidation';
import MapLocation from './mapa';

import { ExclamationCircleOutlined } from "@ant-design/icons";
import CheckPayment from './CheckPayment';

export class RegistrarDonacionDineroAdminPage extends BasePage {
    createPageProperties = (pageApi) => {
        return {
            title: 'Generar Cita',
            // validation: getRegistrarDonacionValidation,
            mode: getRegistrarDonacionMode
        };
    };



    renderPage(pageApi) {

        const getForm = () => pageApi.getComponent('form');
        const getFormGrid = () => pageApi.getComponent('formGrid.grid');

        const formConfig = {
            autoLoad: true,
            layout: 'vertical',
            load: {
                queryId: 95,
                params: {ID_PROCESO: pageApi.getParamId() || 0},
                hideMessage: true, 
                hideMessageError: true
            },
            save:{
                queryId: 93,
                postLink: (resp, values) => '/citas/generar-cita/generar-cita-admin',
                params: {USUARIO: UserUtil.getUserId()}
            },
            fields:[
                { type: 'divider', label: 'Datos de Usuario'},

                { name: 'nombre', label: 'Nombres: ', column: 3, readOnly:true, value: 'Luis' },
                { name: 'apellido_paterno', label: 'Apellido Paterno', column: 3, readOnly:true, value: 'Espino' },
                { name: 'apellido_materno', label: 'Apellido Materno', column: 3, readOnly:true, value: 'Cuadros' },
                { name: 'dni', label: 'N° Documento', column: 3, readOnly:true, value: '71765979' },

                { type: 'divider', label: 'Datos de la Donación'},
                // { type: 'textarea', name:'descripcion_donacion', label: 'Descripción', column: 12},
                { type:'select', name:'id_forma_donacion', label:'Forma de Donación', column: 3,load: {dataList: [{value: 1, label: 'Recojo de Domicilio'},{value: 2, label: 'Envío a Punto de Donaciones'}]}, search: true, allowClear: true,
                    onChange: (value, values, formApi) => {
                        if(value == 1){
                            getForm()?.getConfig()?.fields[7].setVisible(true);
                            getForm()?.getConfig()?.fields[8].setVisible(true);
                            getForm()?.getConfig()?.fields[9].setVisible(true);
                            getForm().setFieldData('direccion_recojo','')
                        }
                        else{
                            getForm()?.getConfig()?.fields[7].setVisible(false);
                            getForm()?.getConfig()?.fields[8].setVisible(false);
                            getForm()?.getConfig()?.fields[9].setVisible(false);
                            getForm().setFieldData('direccion_recojo','Av el derby - Surco')
                        }
                    }
                },

                { type:'select', name:'id_departamento', label:'Departamento', column: 3,load: {dataList: [{value: 1, label: 'Lima'}]}, search: true, allowClear: true,},
                { type:'select', name:'id_provincia', label:'Provincia', column: 3,load: {dataList: [{value: 1, label: 'Lima'}]}, search: true, allowClear: true,},
                { type:'select', name:'id_distrito', label:'Distrito', column: 3,load: {dataList: [{value: 1, label: 'Lima'}]}, search: true, allowClear: true,},

                { name:'direccion_recojo', label: 'Dirección de Recojo' , column: 3},
                { type: 'date', name:'fecha_recojo', label:'Fecha de Recojo / Envío', allowClear: true,  column: 3, format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD},
                { type: 'date', name:'hora_recojo', label:'Hora de Recojo / Envío', allowClear: true,  column: 3, format: DATE_FORMAT.HHmmss, inputFormat: DATE_FORMAT.HHmmss},
                { type: 'dragger', label:'Adjuntar Imagen', name: 'archivo', column: 3, older: 'repository-bvdigital-ma/procesos/solicitud', multiple: 4,
                    onChange: (value, values, formApi) => {
                        let lst = getForm().getFieldApi('archivo').getFileList();
                        // if (value && value.length > 0){
                        //     lst.forEach(file => {
                        //         getS3Grid().addItem({name_file: file.fileId, originalName: file.name, type_file: file.type, path_file: 'repository-bvdigital-ma/procesos/solicitud', id_doc: pageApi?.name?.data?.id_doc},true);
                        //     });
                        // }
                        // getFormAprobacion().reset();
                    } },


            ],
            onComponentLoad : (formApi, pageApi) =>{
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        };


        

        const barButtonconfig = {
            fields:[
                {...BUTTON_DEFAULT.SAVE, label: 'Registrar Donación', onClick: () => {
                    if(getForm().getData().id_forma_donacion == null){
                        return MessageUtil('warning','Alerta!','Debe Seleccionar Forma de Donación!')
                    }
                    if(getForm().getData().id_forma_donacion == 1){
                        if(getForm().getData().id_departamento == null){
                            return MessageUtil('warning','Alerta!','Debe Seleccionar Departamento!')
                        }
                        if(getForm().getData().id_provincia == null){
                            return MessageUtil('warning','Alerta!','Debe Seleccionar Provincia!')
                        }
                        if(getForm().getData().id_distrito == null){
                            return MessageUtil('warning','Alerta!','Debe Seleccionar Distrito!')
                        }

                        if(getForm().getData().direccion_recojo == '' || getForm().getData().direccion_recojo == null){
                            return MessageUtil('warning','Alerta!','Debe ingresar Dirección!')
                        }

                        if(getForm().getData().fecha_recojo == '' || getForm().getData().fecha_recojo == null){
                            return MessageUtil('warning','Alerta!','Debe Ingresar fecha!')
                        }

                        if(getForm().getData().hora_recojo == '' || getForm().getData().hora_recojo == null){
                            return MessageUtil('warning','Alerta!','Debe Ingresar hora!')
                        }
                        
                    }
                    else{

                        if(getForm().getData().direccion_recojo == '' || getForm().getData().direccion_recojo == null){
                            return MessageUtil('warning','Alerta!','Debe ingresar Dirección!')
                        }

                        if(getForm().getData().fecha_recojo == '' || getForm().getData().fecha_recojo == null){
                            return MessageUtil('warning','Alerta!','Debe Ingresar fecha!')
                        }

                        if(getForm().getData().hora_recojo == '' || getForm().getData().hora_recojo == null){
                            return MessageUtil('warning','Alerta!','Debe Ingresar hora!')
                        }

                    }

                    confirm({
                        title: 'Desea Registrar su donación ?',
                        icon: <ExclamationCircleOutlined />,
                        content: '',
                        onOk() {
                            MessageUtil('success','Felicidades!','A registrado su donación con éxito!')
                        },
                        onCancel() {
                        },
                      });
                    
                }},
            ]
        }


        return (
            <>
                <FormFw
                        name="form"
                        getParentApi={pageApi.getApi}
                        config={formConfig}
                />


                <CheckPayment />
{/*
                <div style={{height: '400px'}}>
                    <MapLocation/>
                </div> */}

                <BarButtonFw
                        name="bar"
                        getParentApi={pageApi.getApi}
                        config={barButtonconfig}
                />

            </>
        )

    }
}
