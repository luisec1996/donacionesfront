export const getRegistrarDonacionValidation = () => {
    return {
        form: {
                id_forma_donacion: [{required: true, message: 'La forma de Donación Requerido' }],
                id_departamento: [{required: true, message: 'Departamento es Requerido' }],
                id_provincia: [{required: true, message: 'Provincia es Requerido' }],
                id_distrito: [{required: true, message: 'Distrito es Requerido' }],
                direccion_recojo: [{required: true, message: 'Dirección es Requerido' }],
                fecha_recojo: [{required: true, message: 'Fecha es Requerido' }],
                hora_recojo: [{required: true, message: 'Hora es Requerido' }],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
};