import {Elements} from '@stripe/react-stripe-js';
import {loadStripe} from '@stripe/stripe-js';
import { useEffect, useState } from 'react';
import CheckoutForm from './CheckoutForm';

// Make sure to call `loadStripe` outside of a component’s render to avoid
// recreating the `Stripe` object on every render.
const stripePromise = loadStripe('pk_test_51JsflrEIcJHSMYAWM3ywkAw8OcNRss7ZSQQsqK8DIecxT2yvRQ9JhBsU5TQy12maGl1hP5qxwJJRuCwsWvkgmDce00Mp0soy9B');

const CheckPayment = () => {
  // const options = {
  //   // passing the client secret obtained from the server
  //   clientSecret: 'sk_test_51JsflrEIcJHSMYAWyZLndIbB7vrzun60Gg9sutKgw8pSXw3h3Ko9sfuIx730gzL4Dvts7s2x6QMqSYtlKps91QNT00ugjKX9sS',
  // };

  const [clientSecret, setClientSecret] = useState("");

  useEffect(() => {
    // Create PaymentIntent as soon as the page loads
    fetch("/create-payment-intent", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ items: [{ id: "xl-tshirt" }] }),
    })
      .then((res) => res.json())
      .then((data) => setClientSecret('sk_test_51JsflrEIcJHSMYAWyZLndIbB7vrzun60Gg9sutKgw8pSXw3h3Ko9sfuIx730gzL4Dvts7s2x6QMqSYtlKps91QNT00ugjKX9sS'));
  }, []);

  const appearance = {
    theme: 'stripe',
  };
  const options = {
    clientSecret,
    appearance,
  };

  return (
    <>
    {clientSecret && (
      <Elements options={options} stripe={stripePromise}>
        <CheckoutForm />
      </Elements>
    )}
    </>
  );
};

export default CheckPayment;