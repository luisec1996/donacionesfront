import React, {lazy} from 'react'
import {
    Switch,
    Route
  } from "react-router-dom"


const RegistrarDonacionRoutes = lazy(() => import('./registrar-donacion/RegistrarDonacionRoutes'));
const ConsultarEstadoDonacionRoutes = lazy(() => import('./consultar-estado-donacion/ConsultarEstadoDonacionRoutes'));
const RegistrarDonacionDineroRoutes = lazy(() => import('./registrar-donacion-dinero/RegistrarDonacionDineroRoutes'));

const DonacionesRoutes = () => {
    return (
        <Switch>
            <Route path="/donaciones/registrar-donacion" component={RegistrarDonacionRoutes} />
            <Route path="/donaciones/registrar-donacion-dinero" component={RegistrarDonacionDineroRoutes} />
            <Route path="/donaciones/consultar-estado-donacion" component={ConsultarEstadoDonacionRoutes} />
        </Switch>
    )
}
export default DonacionesRoutes; 

