import React from 'react'
import { Route, Switch } from 'react-router-dom'
import { RegistrarDonacionAdminPage } from './admin/RegistrarDonacionAdminPage';

const RegistrarDonacionRoutes = () => {
    return (
        <Switch>
            <Route exact path="/donaciones/registrar-donacion/registrar-donacion-admin" component={RegistrarDonacionAdminPage} />
        </Switch>
    )
}

export default RegistrarDonacionRoutes
