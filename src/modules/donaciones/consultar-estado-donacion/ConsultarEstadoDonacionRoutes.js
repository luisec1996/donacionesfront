import React from 'react'
import { Route, Switch } from 'react-router-dom'
import { ConsultarEstadoDonacionAdminPage } from './admin/ConsultarEstadoDonacionAdminPage';

const ConsultarEstadoDonacionRoutes = () => {
    return (
        <Switch>
            <Route exact path="/donaciones/consultar-estado-donacion/consultar-estado-donacion-admin" component={ConsultarEstadoDonacionAdminPage} />
        </Switch>
    )
}

export default ConsultarEstadoDonacionRoutes
