import React from 'react';
import { DATE_FORMAT, PAGE_MODE } from '../../../../constants/constants';
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw, { COLUMN_DEFAULT } from '../../../../framework/grid/GridFw';
import { BasePage } from "../../../../framework/pages/BasePage";
import { UserUtil } from '../../../../util/UserUtil';
import { SeguimientoModal } from './modals/SeguimientoModal';

import '../../../../framework/layout/chat/LearningOptions.css'

export class ConsultarEstadoDonacionAdminPage extends BasePage {
    createPageProperties = (pageApi) => {
        return {
            title: 'Consultar Estado Donación'
        };
    };

    renderPage(pageApi) {
        
        const getForm = () => pageApi.getComponent('form');
        const getGrid = () => pageApi.getComponent('grid');

        const DocumentModalConfig = {
                width: 950,
                maskClosable: false,
                component: <SeguimientoModal name={""} getParentApi={pageApi.getApi} />
        };


        const formConfig = {
            autoLoad: true,
            load: {
                queryId: 95,
                params: {ID_PROCESO: pageApi.getParamId() || 0},
                fnOk: () => {
                }
            },
            save:{
                queryId: 93,
                postLink: (resp, values) => '/citas/generar-cita/generar-cita-admin',
                params: {USUARIO: UserUtil.getUserId()}
            },
            fields:[
                { type:'select', name:'id_tipo_donacion', label:'Tipo de Donación',load: {dataList: [{value: 1, label: 'En Espera'},{value: 2, label: 'En Proceso de Envío'},{value: 3, label: 'Donación Entregada'}]}, search: true, allowClear: true,},
                { type: 'date', name:'fecha_recojo', label:'Fecha de Recojo / Envío', allowClear: true, format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD},
            ],
            onComponentLoad : (formApi, pageApi) =>{
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)    
                    formApi.load();
                    
        
            }
        };

        const gridConfig = {
            autoLoad: true,
            load: {
                queryId: 95,
                params: {ID_PROCESO: pageApi.getParamId() || 0},
                fnOk: () => {
                    
                }
            },
            save:{
                queryId: 93,
                postLink: (resp, values) => '/citas/generar-cita/generar-cita-admin',
                params: {USUARIO: UserUtil.getUserId()}
            },
            fields:[
                { name: 'descripcion_donacion', label: 'Descripción', value: 'Luis' },
                { name:'fecha', label: 'Fecha'},
                { name:'estado', label:'Estado'},
                { ...COLUMN_DEFAULT.SETTING, label: 'Opciones', onClick: () =>{ pageApi.showModal(DocumentModalConfig)}}
            ]
        };

        

    
        const barButtonconfig = {
            fields:[
                {...BUTTON_DEFAULT.SEARCH, onClick: () => getGrid().addItems(
                    [
                    {descripcion_donacion: '4 cajas de Leche', fecha: '31/10/2021', estado: 'En Espera'},
                    {descripcion_donacion: '10 sacos de arroz', fecha: '31/10/2021', estado: 'En Proceso de Envío'},
                    {descripcion_donacion: '20 chompas', fecha: '29/10/2021', estado: 'Donación Entregada'}
                    ]
                )},
            ]
        }


        return (
            <>
            



                <FormFw
                        name="form"
                        getParentApi={pageApi.getApi}
                        config={formConfig}
                />
                <BarButtonFw  
                        name="bar"
                        getParentApi={pageApi.getApi}
                        config={barButtonconfig}
                />
                <GridFw
                        name="grid"
                        getParentApi={pageApi.getApi}
                        config={gridConfig}
                />
                

            </>
        )

    }
}
