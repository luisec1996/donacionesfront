import { ExclamationCircleOutlined } from '@ant-design/icons';
import confirm from 'antd/lib/modal/confirm';
import React from 'react';
import { PAGE_MODE } from '../../../../../constants/constants';
import { EnvConstants } from '../../../../../EnvConstants';
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../../framework/bar-button/BarButtonFw';
import FormGridFw, { FORM_GRID_MODE } from '../../../../../framework/form-grid/FormGridFw';
import FormFw from '../../../../../framework/form/FormFw';
import GridFw, { COLUMN_DEFAULT } from '../../../../../framework/grid/GridFw';
import { BasePage } from '../../../../../framework/pages/BasePage';
import { LoadUtil } from '../../../../../framework/util/LoadUtil';
import { MessageUtil } from '../../../../../util/MessageUtil';
import { S3Util } from '../../../../../util/S3Util';
import { UserUtil } from '../../../../../util/UserUtil';
import { DocumentPreview } from '../../../../viewDocument/DocumentPreview';
import MapLocation from './mapa';

export class SeguimientoModal extends BasePage {

    createPageProperties = (pageApi) => {
        return {
            title: 'Seguimiento de Envío',
            barHeader: {
                fields:[
                    {...BUTTON_DEFAULT.RETURN, onClick: () => pageApi.hideThisModal()}
                ]
            }
        };
    }
    
    renderPage(pageApi) {

        return (
            <>
                <MapLocation/>
            </>
        )

    }
    
}