import React, { lazy, Suspense } from 'react'
import {
    Switch,
    Route,
    useRouteMatch
  } from "react-router-dom"
import { LoadingPage } from '../framework/loading/LoadingPage';

const DonacionesRoutes = lazy(() => import('./donaciones/DonacionesRoutes'));

export const ModulesRoutes = () => {
    const { path } = useRouteMatch();
    return (
         <Suspense fallback={<LoadingPage/>}>
            <Switch>
         
                <Route path={`${path}donaciones`} component={DonacionesRoutes}/>
           
            </Switch>
         </Suspense>
    )
}

//https://learnwithparam.com/blog/how-to-handle-query-params-in-react-router/