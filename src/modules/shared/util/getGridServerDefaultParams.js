import { EnvConstants } from "../../../EnvConstants";

export const getGridServerDefaultParams = (config) => {
    return {
        autoLoad: true,
        pagination: {//true or object definition
            server: true
        },
        load: {
            url: EnvConstants.GET_FW_GRID_URL_LOAD_BASE(),
            queryId: config['queryLoad'],
            orderBy: config['order'],
            params: config['params'],
            fnOk: (resp) => {
                console.log(``);
            },
            hideMessage: true, 
            hideMessageError: true
        }
    };
}
