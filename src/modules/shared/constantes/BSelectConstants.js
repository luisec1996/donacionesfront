import { QueryCache } from "./QueryCache";

export const B_SELECT_CONSTANTS = {
    SEXO: {type:'select', name:'sexo', label:'Sexo', column: 3, load: {queryId: QueryCache.SEX0.queryId}},
    SEDE: {type:'select', name:'Sede', label:'Sede', column: 3, load: {queryId: 47, cache: true}},
    EMPRESA: {type:'select', name:'Empresa', label:'Empresa', column: 3, load: {queryId: 1, cache: true}},
    DIVISION: {type:'select', name:'Division', label:'División', column: 3, load: {queryId: 12}, parents: [
                    {name:'Empresa', paramName: 'empresaId', required: true}
    ]},
    AREA: {type:'select', name:'Area', label:'Área', column: 3, load: {queryId: 42}, parents: [
                    {name:'Division', paramName: 'divisionId', required: true}                    
    ]}
};