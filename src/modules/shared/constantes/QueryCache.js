export const QueryCache = {
    SEX0: {
        queryId: -1,
        dataList: [
            {label: 'Masculino', value: 'M'},
            {label: 'Femenino', value: 'F'},
            {label: 'Indefinido', value: 'I'}
        ]
    },
    TIPO_DOCUMENTO: {//combo se sexo
        queryId: -2,
        dataList: [
            {label: 'DNI', value: '1'},
            {label: 'Pasaporte', value: '2'}
        ]    
    },
    EMPRESA: {queryId: 1}
};
