# FRAMEWORK FRONT 
Framework basado en 'React' y 'Ant Design' para desarrollo rapido de aplicaciones 
## REACT => [https://es.reactjs.org/](https://es.reactjs.org/) 
## ANT DESIGN => [https://ant.design/components/overview/](https://ant.design/components/overview/)




## USO

### MODO LOCAL
* Crear un archivo .env.local
`REACT_APP_URL_BASE_BACKEND=https://localhost:5001`
* Instalar dependencias
`npm install`
* Ejecutar comando:
`npm start`


### MODO LOCAL
* Ejecutar comando:
`npm run build`





